FROM openjdk:8-jre-alpine as builder
WORKDIR build
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} genius-sports-consumer.jar
RUN java -Djarmode=layertools -jar genius-sports-consumer.jar extract

FROM openjdk:8-jre-alpine
VOLUME /tmp
WORKDIR application
COPY --from=builder build/dependencies/ ./
COPY --from=builder build/spring-boot-loader/ ./
COPY --from=builder build/snapshot-dependencies/ ./
COPY --from=builder build/application/ ./
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} org.springframework.boot.loader.JarLauncher ${0} ${@}"]
