# Genius Sports Consumer

This is an implementation that will Consume messages specific for Genius Sports (a.k.a. Bet Genius), which is replacing Bet Radar.

Messages will be sent by Genius Sports to their required endpoints
exposed by this Consumer (see specification in [Genius
Sports Integration Service Interface](https://geniussports.atlassian.net/wiki/spaces/BID/pages/34822507/Integration+Service+Interface)):
```
interface ITradingPlatform
{   
    void   ProcessMessage(string messageXml);   
    void   Heartbeat();
} 
```

## Running Locally

To run the application locally simply run the following commands:

**Generate Jar**

*Linux* ``./mvnw clean verify``

*Windows* ``.\mvn.cmd clean verify``

**Run Standalone**

*Linux* ``./mvnw spring-boot:run``

*Windows* ``.\mvn.cmd spring-boot:run``

**Build and Run Container**
```
docker build -t donbest/genius-sports-consumer .

docker run -p 80:80 -e "JAVA_OPTS=-Xmx128m" --name genius_sports_consumer donbest/genius-sports-consumer --server.port=80
```

### Deployment Servers
**DEV**  dbs02widget1.donbest.com:/dbs/betgenius-sports-consumer

**PROD**  dbs02sportspuncher1.donbest.com:/dbs/betgenius-sports-consumer
