package com.donbest.betgenius.consumer.integration_test;

import com.donbest.betgenius.consumer.cache.FixtureIdCache;
import com.donbest.betgenius.consumer.handlers.NCAABHandler;
import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.consumer.service.BetGeniusFixtureService;
import com.donbest.betgenius.consumer.service.ProcessMessageService;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.sportsdataservices.schemas.integrationservice.v3.Fixture;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_XML;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(setupBefore = TestExecutionEvent.TEST_EXECUTION)
@DirtiesContext
public class DonBestBetGeniusConsumerApplicationITTest {

    private static final Logger logger = LoggerFactory.getLogger(DonBestBetGeniusConsumerApplicationITTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProcessMessageService processMessageService;

    @Autowired
    private FixtureIdCache fixtureIdCache;

    @MockBean
    private BetGeniusFixtureService betGeniusFixtureService;

    @MockBean
    private NCAABHandler ncaabHandler;

    @Autowired
    protected UserDetailsService getMockUserDetailsService() {
        UserDetails user =
                User.withUsername("user")
                        .password("{bcrypt}$2a$10$9C.lbXi0rp2u2pU20oHspOMDyCxa.xiUOXLrI3Fn8yh1yPR7T4gMy")
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }

    @Test
    //@Disabled("Run only if explicitly required.")
    void testProcessTwoXMLMessagesSameFixtureId() throws Exception {
        when(betGeniusFixtureService.updateFixture(eq(String.valueOf(8403369)), any(Fixture.class)))
                .thenAnswer((Answer<EventRotation>) invocation -> {
                    EventRotation eventRot = new EventRotation();
                    eventRot.setEventId(Integer.valueOf(invocation.getArgument(0, String.class)));
                    eventRot.setAwayRot(0);
                    eventRot.setAwayTeam("Away Team");
                    eventRot.setHomeTeam("Home Team");
                    eventRot.setFlip(true);
                    eventRot.setLeagueId(League.NCAAB.getId());
                    Fixture fixture = null;
                    Integer fixtureId = null;
                    try {
                        fixture = invocation.getArgument(1, Fixture.class);
                        if (fixture != null) {
                            eventRot.setStartTime(fixture.getStartTimeUtc().toGregorianCalendar().getTime().getTime());
                            fixtureId = fixture.getId();
                            Thread.sleep(1000);
                        }
                    } catch (Exception e) {
                        logger.error("Unexpected exception from testing ProcessMessage with FixtureId={}", fixtureId, e);
                    }
                    return eventRot;
                });

        doNothing().when(ncaabHandler).handle(any(League.class), any(EventRotation.class), any(Updategram.class));

        final String requestEmptyFixtureStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<Updategram xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3\">"
                + "<Header><MessageGuid>5d2805e3-bfe1-4f8f-bacd-3f7c5bc63d68</MessageGuid><TimeStampUtc>2022-03-16T02:04:40.7384132Z</TimeStampUtc></Header>"
                + "<Fixture>"
                + "<Id>8403369</Id>"
                + "<StartTimeUtc>2022-03-17T01:40:00Z</StartTimeUtc>"
                + "</Fixture>"
                + "</Updategram>";

        final String requestEmptyBasketballMatchSummaryStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<Updategram xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3\">"
                + "<Header><MessageGuid>6e3916f4-c0f2-509e-cbde-408d6cd74e79</MessageGuid><TimeStampUtc>2022-03-16T02:04:40.8495243Z</TimeStampUtc></Header>"
                + "<BasketballMatchSummary xmlns:ns2=\"http://schemas.sportsdataservices.com/basketball/matchstate/external/v2\">"
                + "<ns2:FixtureId>8403369</ns2:FixtureId>"
                + "<ns2:MessageTimestampUtc>2022-03-17T01:40:00Z</ns2:MessageTimestampUtc>"
                + "</BasketballMatchSummary>"
                + "</Updategram>";

        this.mockMvc.perform(post("/processMessage")
                        .accept(APPLICATION_XML)
                        .contentType(APPLICATION_XML)
                        .content(requestEmptyFixtureStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_XML))
                .andExpect(content().xml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<UpdategramResponse xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3/response\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                        + "<Header xsi:nil=\"true\"/>"
                        + "<MarketSet xsi:nil=\"true\"/>"
                        + "</UpdategramResponse>"));

        this.mockMvc.perform(post("/processMessage")
                        .accept(APPLICATION_XML)
                        .contentType(APPLICATION_XML)
                        .content(requestEmptyBasketballMatchSummaryStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_XML))
                .andExpect(content().xml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<UpdategramResponse xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3/response\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                        + "<Header xsi:nil=\"true\"/>"
                        + "<MarketSet xsi:nil=\"true\"/>"
                        + "</UpdategramResponse>"));
    }

}
