package com.donbest.betgenius.consumer.controller;

import com.donbest.betgenius.consumer.service.HeartbeatService;
import com.donbest.betgenius.consumer.service.ProcessMessageService;
import com.donbest.betgenius.consumer.utils.RawDataConfig;
import com.donbest.betgenius.consumer.utils.RawDataUtils;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import com.sportsdataservices.schemas.integrationservice.v3.response.UpdategramResponse;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TradingPlatformController.class)
@WithMockUser(setupBefore = TestExecutionEvent.TEST_EXECUTION)
class TradingPlatformControllerTest {

    static final Logger logger = LoggerFactory.getLogger(TradingPlatformControllerTest.class);

    private final com.sportsdataservices.schemas.integrationservice.v3.response.ObjectFactory
            responseObjectFactory = new com.sportsdataservices.schemas.integrationservice.v3.response.ObjectFactory();

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HeartbeatService heartbeatService;

    @MockBean
    private ProcessMessageService processMessageService;

    @MockBean
    private RawDataConfig rawDataConfig;

    @MockBean
    private RawDataUtils rawDataUtils;

    @Test
    @WithAnonymousUser
    void testHeartbeatUnauthenticated() throws Exception {
        when(heartbeatService.heartbeat())
                .thenReturn("");

        this.mockMvc.perform(post("/heartbeat")
                        .accept(TEXT_PLAIN)
                        .contentType(TEXT_PLAIN)
                        .content(""))
                .andExpect(unauthenticated())
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testHeartbeatAuthenticated() throws Exception {
        when(heartbeatService.heartbeat())
                .thenReturn("");

        this.mockMvc.perform(post("/heartbeat")
                        .accept(TEXT_PLAIN)
                        .contentType(TEXT_PLAIN)
                        .content(""))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_PLAIN))
                .andExpect(content().string(""));
    }

    @Test
    void testProcessMessageXML() throws Exception {
        when(processMessageService.processMessage(any(Updategram.class)))
                .thenAnswer((Answer<UpdategramResponse>) invocation -> responseObjectFactory.createUpdategramResponse());

        final String requestStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<Updategram xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3\">"
                + "<Header><MessageGuid>5d2805e3-bfe1-4f8f-bacd-3f7c5bc63d68</MessageGuid></Header>"
                + "</Updategram>";

        this.mockMvc.perform(post("/processMessage")
                        .accept(APPLICATION_XML)
                        .contentType(APPLICATION_XML)
                        .content(requestStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_XML))
                .andExpect(content().xml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<UpdategramResponse xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3/response\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                        + "<Header xsi:nil=\"true\"/>"
                        + "<MarketSet xsi:nil=\"true\"/>"
                        + "</UpdategramResponse>"));
    }

    @Test
    void testProcessMessageJSON() throws Exception {
        when(processMessageService.processMessage(any(Updategram.class)))
                .thenAnswer((Answer<UpdategramResponse>) invocation -> responseObjectFactory.createUpdategramResponse());

        final String requestStr = "{ \"Header\": { \"MessageGuid\": \"659b5cd7-75ac-4c6c-98b5-6363decbdc26\" } }";

        this.mockMvc.perform(post("/processMessage")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(requestStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(content().json("{\"header\":null,\"marketSet\":null}"));
    }

}
