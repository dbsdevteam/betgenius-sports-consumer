package com.donbest.betgenius.consumer;

import com.donbest.betgenius.consumer.service.ProcessMessageService;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import com.sportsdataservices.schemas.integrationservice.v3.response.UpdategramResponse;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(setupBefore = TestExecutionEvent.TEST_EXECUTION)
public class DonBestBetGeniusConsumerApplicationTest {

    private static final Logger logger = LoggerFactory.getLogger(DonBestBetGeniusConsumerApplicationTest.class);

    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private ProcessMessageService processMessageService;

    protected UpdategramResponse getEmptyUpdategramResponse() {
    	UpdategramResponse response = new UpdategramResponse();
    	response.setHeader(null);
    	response.setMarketSet(null);

        return response;
    }
    
    @Autowired
    protected UserDetailsService getMockUserDetailsService() {
        UserDetails user =
                User.withUsername("user")
                        .password("{bcrypt}$2a$10$9C.lbXi0rp2u2pU20oHspOMDyCxa.xiUOXLrI3Fn8yh1yPR7T4gMy")
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }

    @Test
    @WithAnonymousUser
    public void testHeartbeatUnauthenticated() throws Exception {
        this.mockMvc.perform(post("/heartbeat")
                        .accept(ALL_VALUE))
                .andExpect(unauthenticated())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testHeartbeatAuthenticated() throws Exception {
        this.mockMvc.perform(post("/heartbeat")
                        .accept(ALL_VALUE))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_PLAIN))
                .andExpect(content().string(""));
    }

    @Test
    void testProcessMessageXML() throws Exception {
        final String requestStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<Updategram xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3\">"
                + "<Header><MessageGuid>5d2805e3-bfe1-4f8f-bacd-3f7c5bc63d68</MessageGuid></Header>"
                + "</Updategram>";
        
        when(processMessageService.processMessage(any(Updategram.class))).thenReturn(getEmptyUpdategramResponse());
       
        this.mockMvc.perform(post("/processMessage")
                        .accept(TEXT_XML)
                        .contentType(TEXT_XML)
                        .content(requestStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_XML))
                .andExpect(content().xml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<UpdategramResponse xmlns=\"http://schemas.sportsdataservices.com/integrationService/v3/response\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                        + "<Header xsi:nil=\"true\"/>"
                        + "<MarketSet xsi:nil=\"true\"/>"
                        + "</UpdategramResponse>"));
    }

    @Test
    void testProcessMessageJSON() throws Exception {
        final String requestStr = "{ \"Header\": { \"MessageGuid\": \"659b5cd7-75ac-4c6c-98b5-6363decbdc26\" } }";

        when(processMessageService.processMessage(any(Updategram.class))).thenReturn(getEmptyUpdategramResponse());
        
        this.mockMvc.perform(post("/processMessage")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(requestStr))
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(content().json("{\"header\":null,\"marketSet\":null}"));
    }

}
