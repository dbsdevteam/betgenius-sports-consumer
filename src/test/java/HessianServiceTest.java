import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

import com.caucho.hessian.client.HessianProxyFactory;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.donbest.betgenius.hattrick.service.BridgeService;

public class HessianServiceTest {

    private static final SimpleDateFormat sdfDateAndTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static String svcUrl = "http://dbs02xml6b.donbest.com:8080/hattrick-parser-supporter/remoting/bridgeService.svc";

    @Test
    public void test() {

        BridgeService svc = null;

        try {
            System.out.println("os.name=" + System.getProperty("os.name"));

            HessianProxyFactory factory = new HessianProxyFactory();
            svc = (BridgeService) factory.create(BridgeService.class, svcUrl);

            GregorianCalendar calendar=new GregorianCalendar(2022, 2, 12);	// month index 0 based
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
//			calendar.set(Calendar.MILLISECOND, 0);
//			calendar.set(Calendar.HOUR, 12);

            Date gameDate=calendar.getTime();

            // league: 1:NFL, 3:NBA, 4:NCAAB, 5:MLB, 7:NHL, 9:soccer, 10:boxing, 11:golf, 12:tennis, 14:auto, 109:table tennis
            // 99:esports, 1003:CS-GO, 1000: Dota2
            Integer sportId = 2;
            Integer leagueId = 4;
            String awayName = "Montana State Bobcats";
            String homeName = "Weber State Wildcats"; //"Atlético Tucumán".replace("á", "a").replace("é", "e");
            EventRotation er = null;

//			er = svc.getEventRotationByLeaugeIdAndTeamName(leagueId, awayName, homeName);
//			er = svc.getEventIdByLeaugeIdAndTeamName(leagueId, awayName, homeName, gameDate);
//			er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDate(leagueId, awayName, homeName, gameDate);
//			er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDateDoubleHeader(leagueId, awayName, homeName, gameDate, 0);
            er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDateHourGap(leagueId, awayName, homeName, gameDate, 2);
//			er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDateMinutesGap(leagueId, awayName, homeName, gameDate, 15);
//			er = svc.getEventIdBySportIdAndTeamName(sportId, awayName, homeName, gameDate);
//			er = svc.getPropositionByLeaugeIdAndTeamName(leagueId, awayName, homeName, gameDate);
//			er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDateGolf(leagueId, awayName, homeName, gameDate, 4);
//			er = svc.getEventRotationByLeaugeIdAndTeamNameAndGameDateAndHeaderPortion(leagueId, awayName, homeName, gameDate, "MLB SERIES PRICES");

            System.out.println("sportId = " + sportId + " or leagueId = " + leagueId + ", away = " + awayName + ", home = " + homeName + ", date = " + sdfDateAndTime.format(gameDate));

            if(er == null)
            {
                System.out.println("EventRotation returns null.\n");
            }
            else
            {
                System.out.println("EventRotation returns:");
                System.out.println("eId = " + er.getEventId());
                System.out.println("rot = " + er.getAwayRot());
                System.out.println(er.isFlip() ? "Flipped" : "Not flipped");
                System.out.println("awayTeam = " + er.getAwayTeam());
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
}
