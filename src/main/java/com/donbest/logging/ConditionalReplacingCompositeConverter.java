package com.donbest.logging;

import ch.qos.logback.core.pattern.CompositeConverter;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class ConditionalReplacingCompositeConverter<E> extends CompositeConverter<E> {

    public static final Marker confidential = MarkerFactory.getMarker("CONFIDENTIAL");

    private final Map<Pattern, String> patterns = new HashMap<Pattern, String>();

    /**
     * Logback custom converters can override the start() method in order to be properly initialized.
     * In this case, we use it to pre-compile the regular expression(s).
     */
    @Override
    public void start() {
        Properties properties = null;
        try {
            properties = PropertiesLoaderUtils.loadAllProperties("logging.properties");
        } catch (IOException e) {
            addError("couldn't load properties from logging.properties file");
            return;
        }

        String patternsProperty = properties.getProperty("logging.patterns", "");
        for (String patternProp : patternsProperty.split(",")) {
            String[] pattern = patternProp.split(" REPLACE ");
            if (pattern.length > 1) {
                try {
                    patterns.put(Pattern.compile(pattern[0]), pattern[1]);
                } catch(PatternSyntaxException pse) {
                    addError("Invalid replacement pattern: " + patternProp, pse);
                }
            }
        }

        super.start();
    }

    @Override
    protected String transform(E event, String in) {
        for (Map.Entry<Pattern, String> entry : patterns.entrySet()) {
            Pattern pattern = entry.getKey();
            in = pattern.matcher(in).replaceAll(entry.getValue());
        }

        return in;
    }

}
