package com.donbest.betgenius.hattrick.service;

import java.util.Date;

import com.donbest.betgenius.hattrick.model.EventRotation;

public interface BridgeService {
    EventRotation getEventRotationByLeaugeIdAndTeamNameAndGameDateHourGap(Integer leagueId, String awayTeamName, String homeTeamName, Date scheduleOpenTime, Integer hourGap);
}
