package com.donbest.betgenius.hattrick.model;

import lombok.Getter;
import lombok.Setter;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Setter
@Getter
@Root(name="matchup_score")
public class BBMatchupScore implements Serializable
{
	@Attribute(name="event_id")
	private Integer eventId;
	
	@Attribute(name="away_rotation")
	private Integer awayRotation;

	@Attribute(name="away_score")
	private String awayScore;

	@Attribute(name="home_rotation")
	private Integer homeRotation;

	@Attribute(name="home_score")
	private String homeScore;

	@Attribute(name="sequence")
	private Integer sequence;

	@Attribute(name="period")
	private String period;

	@Attribute(name="description")
	private String description;

	@Attribute(name="final")
	private String isFinal;

	@Attribute(name="possession")
	private Integer possession;

	@Attribute(name="possession_team")
	private String possessionTeam;

	@Attribute(name="ft")
	private String freeThrow;
	
	@Attribute(name="homeFT")
	private Integer homeFT;
	
	@Attribute(name="awayFT")
	private Integer awayFT;
	
	@Attribute(name="pause")
	private Integer pause;
	
}
