package com.donbest.betgenius.hattrick.model;

import java.io.Serializable;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


@Root(name="scoring-update")
public class ScoreUpdate implements Serializable
{
	@Attribute(name="eventId")
	private Integer eventId;
	
	@Attribute(name="leagueId", required=false)
	private Integer leagueId;
	
	@Attribute(name="period")
	private Integer period;
	
	@Attribute(name="time")
	private String time;
	
	@Attribute(name="halfTime")
	private String halfTime;
	
	@Attribute(name="finalScore")
	private String finalScore = "false";	// always false, Sports will send final message
	
	@Attribute(name="correction")
	private String correction;
	
	@Attribute(name="periodEnd")
	private String periodEnd;
	
	@Attribute(name="messageSource")
	private String messageSource;
	
	@Element(name="situational-text", required=false)
	private String situationText;
	
	@ElementList(name="score", inline=true)
	private List<Score> scoreList;

	public Integer getEventId()
	{
		return eventId;
	}

	public void setEventId(Integer eventId)
	{
		this.eventId = eventId;
	}

	public Integer getLeagueId()
	{
		return leagueId;
	}

	public void setLeagueId(Integer leagueId)
	{
		this.leagueId = leagueId;
	}

	public Integer getPeriod()
	{
		return period;
	}

	public void setPeriod(Integer period)
	{
		this.period = period;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public String getHalfTime()
	{
		return halfTime;
	}

	public void setHalfTime(String halfTime)
	{
		this.halfTime = halfTime;
	}

	public String getFinalScore()
	{
		return finalScore;
	}

	public void setFinalScore(String finalScore)
	{
		this.finalScore = finalScore;
	}

	public String getCorrection()
	{
		return correction;
	}

	public void setCorrection(String correction)
	{
		this.correction = correction;
	}

	public String getPeriodEnd()
	{
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd)
	{
		this.periodEnd = periodEnd;
	}

	public String getMessageSource()
	{
		return messageSource;
	}

	public void setMessageSource(String messageSource)
	{
		this.messageSource = messageSource;
	}

	public String getSituationText()
	{
		return situationText;
	}

	public void setSituationText(String situationText)
	{
		this.situationText = situationText;
	}

	public List<Score> getScoreList()
	{
		return scoreList;
	}

	public void setScoreList(List<Score> scoreList)
	{
		this.scoreList = scoreList;
	}
	
}
