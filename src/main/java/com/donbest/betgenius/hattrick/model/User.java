package com.donbest.betgenius.hattrick.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class User {

	private String user;
	private String password;
	private String refreshtoken;
}