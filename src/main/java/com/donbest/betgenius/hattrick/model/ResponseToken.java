package com.donbest.betgenius.hattrick.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseToken implements Serializable  {

	@JsonProperty("IdToken")
	private String IdToken;
	
	@JsonProperty("AccessToken")
	private String AccessToken;
	
	@JsonProperty("RefreshToken")
	private String RefreshToken;
	
	@JsonProperty("TokenType")
	private String TokenType;
	
	@JsonProperty("ExpiresIn")
	private Long ExpiresIn;
	
	
	
	
}
