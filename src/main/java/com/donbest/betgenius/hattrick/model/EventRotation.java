package com.donbest.betgenius.hattrick.model;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class EventRotation implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer eventId;
    private Integer awayRot;
    private String awayTeam;
    public String homeTeam;
    private boolean flip;
    public Integer leagueId;
    public Long startTime;

}
