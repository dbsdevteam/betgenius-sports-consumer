package com.donbest.betgenius.hattrick.model;

import java.io.Serializable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="score")
public class Score implements Serializable
{
	@Attribute(name="rotNum")
	private Integer rotNum;
	
	@Text
	private String scoreText;

	public Integer getRotNum()
	{
		return rotNum;
	}

	public void setRotNum(Integer rotNum)
	{
		this.rotNum = rotNum;
	}

	public String getScoreText()
	{
		return scoreText;
	}

	public void setScoreText(String scoreText)
	{
		this.scoreText = scoreText;
	}
}
