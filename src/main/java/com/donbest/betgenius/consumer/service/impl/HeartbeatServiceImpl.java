package com.donbest.betgenius.consumer.service.impl;

import com.donbest.betgenius.consumer.service.HeartbeatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
public class HeartbeatServiceImpl implements HeartbeatService {

    private static final Logger logger = LoggerFactory.getLogger(HeartbeatServiceImpl.class);

    public HeartbeatServiceImpl() {
    }

    @Override
    @PreAuthorize("authenticated")
    public String heartbeat() {
        logger.info("Received heartbeat");
        return null;
    }

}
