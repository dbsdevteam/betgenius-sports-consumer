package com.donbest.betgenius.consumer.service;

import java.util.Date;

import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.sportsdataservices.schemas.integrationservice.v3.Fixture;

public interface BetGeniusFixtureService {

	EventRotation updateFixture(String matchId, Fixture fixture);

	void insertUpdateEventRotDB(String matchId, String awayTeamName, String homeTeamName, League league, Date scheduled_time, EventRotation eventRotation);

}
