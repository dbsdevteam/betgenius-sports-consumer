package com.donbest.betgenius.consumer.service.impl;


import java.util.Date;

import com.donbest.betgenius.consumer.cache.FixtureIdCache;
import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.consumer.service.BetGeniusFixtureService;
import com.donbest.betgenius.consumer.service.ProcessMessageService;
import com.donbest.betgenius.consumer.utils.Utils;
import com.sportsdataservices.schemas.basketball.matchstate.external.v2.BasketballMatchSummary;
import com.sportsdataservices.schemas.integrationservice.v3.Fixture;
import com.sportsdataservices.schemas.integrationservice.v3.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.donbest.betgenius.consumer.handlers.NCAABHandler;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.donbest.betgenius.hattrick.service.BridgeService;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import com.sportsdataservices.schemas.integrationservice.v3.response.ObjectFactory;
import com.sportsdataservices.schemas.integrationservice.v3.response.UpdategramResponse;

import static com.donbest.betgenius.consumer.model.common.League.NCAAB;

@Component
public class ProcessMessageServiceImpl implements ProcessMessageService {

    private static final Logger logger = LoggerFactory.getLogger(ProcessMessageServiceImpl.class);

    public static final int NCAAB_LEAGUE_ID = 4;
    public static final String NCAAB_LEAGUE_NAME = "NCAAB";
    public static final String SPORT_NAME_BASKETBALL = "Basketball";
    public static final String CATEGORY_USA = "USA";

    private final ObjectFactory updategramResponseObjectFactory = new ObjectFactory();
    
    private final BridgeService bridgeService;
    
    private final NCAABHandler ncaabHandler;
    
    private final BetGeniusFixtureService betGeniusFixtureService;

    public ProcessMessageServiceImpl(BridgeService bridgeService, NCAABHandler ncaabHandler, BetGeniusFixtureService betGeniusFixtureService, FixtureIdCache fixtureIdCache) {
        this.bridgeService = bridgeService;
        this.ncaabHandler = ncaabHandler;
        this.betGeniusFixtureService = betGeniusFixtureService;
        this.fixtureIdCache = fixtureIdCache;
    }

    private final FixtureIdCache fixtureIdCache;

    @Override
    @PreAuthorize("authenticated")
    public UpdategramResponse processMessage(Updategram message) {
        logger.info("Processing Message, {}", (message == null ? "null" : toMessageHeader(message)));
        
        if (message != null) {
            Integer fixtureId = null;
            Fixture fixture = null;
            BasketballMatchSummary basketballMatchSummary = null;

            if(message.getFixture() != null) { // Initial Fixture message
                fixture = message.getFixture();
                fixtureId = fixture.getId();
            } else if(message.getBasketballMatchSummary() != null) { // Score message
                basketballMatchSummary = message.getBasketballMatchSummary();
                fixtureId = basketballMatchSummary.getFixtureId();
            }
            if (fixtureId != null) {
                synchronized (fixtureIdCache.get(fixtureId)) { // Make sure to process only one message per fixtureId at a time
                    try {
                        EventRotation eventRot = betGeniusFixtureService.updateFixture(String.valueOf(fixtureId), fixture);
                        if (basketballMatchSummary != null) { // Only handle if actual score message
                            if (eventRot != null) { // Only handle if eventRot is not null
                                League league = League.valueOfId(eventRot.getLeagueId());
                                switch (league) {
                                    case NCAAB:
                                    case WNCAAB:
                                        ncaabHandler.handle(league, eventRot, message);
                                        break;
                                    case UNKNOWN:
                                        logger.warn("Unknown/Unmapped League {} for fixtureId={}", league, fixtureId);
                                        break;
                                }
                            } else {
                                logger.info("eventRot is null, ignore handling, fixtureId={}", fixtureId);
                            }
                        }
                    } finally {
                        fixtureIdCache.evict(fixtureId);
                    }
                }
            }else {
                //other messages, ignore
                logger.info("ignore messages, {}", toMessageHeader(message));
            }
        }
        return updategramResponseObjectFactory.createUpdategramResponse();
    }
    
    @Override
    public EventRotation getEventRotation(Integer leagueId, String awayTeamName, String homeTeamName, Date scheduleOpenTime, Integer hourGap) {
    	logger.info("Fetching EventRotation from Donbest: for league-{}, awayTeamName-{}, homeTeamName-{}, scheduleOpenTime-{} ", leagueId,awayTeamName,homeTeamName,scheduleOpenTime);
    	EventRotation eventRot = bridgeService.getEventRotationByLeaugeIdAndTeamNameAndGameDateHourGap(leagueId, awayTeamName, homeTeamName, scheduleOpenTime, 3);
    	if(eventRot == null) {
    		logger.error("Event Rotation is not available at donbest: for league-{}, awayTeamName-{}, homeTeamName-{}, scheduleOpenTime-{} ", leagueId,awayTeamName,homeTeamName,scheduleOpenTime);
    	}
    	return eventRot;
    }

    private String toMessageHeader(Updategram updategram) {
        Header header = updategram.getHeader();
        if(header != null) {
            return "messageGuid: " + header.getMessageGuid() + " timeStampUtc: "
                    + (header.getTimeStampUtc() != null ? Utils.convertXMLGregorianCalendarToDateString(header.getTimeStampUtc()) : null);
        }
        return "No Header";
    }
}
