package com.donbest.betgenius.consumer.service;

public interface EmailSvc {
	public void sendEmail(String from, String[] to, String[] cc, String subject, String text);
}