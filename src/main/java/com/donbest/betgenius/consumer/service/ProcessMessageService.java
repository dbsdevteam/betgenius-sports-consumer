package com.donbest.betgenius.consumer.service;


import com.donbest.betgenius.hattrick.model.EventRotation;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import com.sportsdataservices.schemas.integrationservice.v3.response.UpdategramResponse;

import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public interface ProcessMessageService {

    UpdategramResponse processMessage(Updategram message);
    EventRotation getEventRotation(Integer leagueId, String awayTeamName, String homeTeamName, Date scheduleOpenTime, Integer hourGap);

}
