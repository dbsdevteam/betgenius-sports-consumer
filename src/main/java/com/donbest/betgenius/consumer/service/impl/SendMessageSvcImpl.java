package com.donbest.betgenius.consumer.service.impl;

import com.donbest.betgenius.consumer.service.SendMessageSvc;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component("sendMessageService")
public class SendMessageSvcImpl implements SendMessageSvc {
	final
	JmsTemplate jmsTemplate;

	public SendMessageSvcImpl(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	@Override
	public boolean send(String destinationName, String message, boolean pubSubDomain) {
		jmsTemplate.setPubSubDomain(pubSubDomain);
		try {
			jmsTemplate.convertAndSend(destinationName, message);
			return true;
		} catch (JmsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}