package com.donbest.betgenius.consumer.service.impl;

import java.io.Serializable;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import com.donbest.betgenius.consumer.cache.MatchupScoreXMLCache;
import com.donbest.betgenius.consumer.cache.ScoreUpdateCache;
import com.donbest.betgenius.consumer.cache.ScoreXMLCache;
import com.donbest.betgenius.consumer.model.common.ScoreDetails;
import com.donbest.betgenius.consumer.service.SendMessageSvc;
import com.donbest.betgenius.hattrick.model.*;
import com.sportsdataservices.schemas.basketball.matchstate.external.v2.PossessionTeamSide;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.donbest.betgenius.consumer.fixtures.model.FixtureResponse;
import com.donbest.betgenius.consumer.service.BetGeniusService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class BetGeniusServiceImpl implements BetGeniusService {

	@Bean
	public RestTemplate restTemplate() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(null, null, null);

		CloseableHttpClient httpClient = HttpClientBuilder.create().setSslcontext(context).build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
		this.restTemplate = new RestTemplate(factory);
		
		//this.restTemplate = new RestTemplate();
		return this.restTemplate;
	}

	private static final Logger logger = LoggerFactory.getLogger(BetGeniusServiceImpl.class);

	private RestTemplate restTemplate;

	@Value("${betgenius.auth_url}")
	private String AUTHENTICATION_URL;

	@Value("${betgenius.fixture_url}")
	private String FIXTURE_URL;

	@Value("${betgenius.refresh_token}")
	private String REFRESH_TOKEN;

	@Value("${betgenius.username}")
	private String USERNAME;

	@Value("${betgenius.pwd}")
	private String PWD;
	
	@Value("${betgenius.x-api-key}")
	private String X_API_KEY;

	private String token;

	@Value("${scoremessage.source}")
	private String scoreMsgSource;

	final
	ScoreUpdateCache scoreUpdateCache;

	final
	ScoreXMLCache scoreXmlCache;

	final
	MatchupScoreXMLCache matchupScoreXMLCache;

	final
	SendMessageSvc sendMessageService;

	public BetGeniusServiceImpl(ScoreUpdateCache scoreUpdateCache, ScoreXMLCache scoreXmlCache, MatchupScoreXMLCache matchupScoreXMLCache, SendMessageSvc sendMessageService) {
		this.scoreUpdateCache = scoreUpdateCache;
		this.scoreXmlCache = scoreXmlCache;
		this.matchupScoreXMLCache = matchupScoreXMLCache;
		this.sendMessageService = sendMessageService;
	}

	@Override
	public String getAuthToken() {
		logger.info("Fetching Auth Token");
		try {
			User authenticationUser = User.builder().user(USERNAME).password(PWD).build();
			String authenticationBody = new ObjectMapper().writeValueAsString(authenticationUser);
			HttpHeaders authenticationHeaders = getHeaders();
			HttpEntity<String> authenticationEntity = new HttpEntity<>(authenticationBody, authenticationHeaders);
			//HttpEntity<ResponseToken> authenticationEntity = new HttpEntity<>(new ResponseToken());
			//Authenticate User and get token
			ResponseEntity<ResponseToken> authenticationResponse = restTemplate.exchange(AUTHENTICATION_URL, HttpMethod.POST, authenticationEntity, ResponseToken.class);
			
			//authentication successful
			if (authenticationResponse.getStatusCode().equals(HttpStatus.OK)&&authenticationResponse.getBody()!=null) {
				token = authenticationResponse.getBody().getIdToken();
				logger.debug("Retrieved Token -{}",token);
			}else {
				logger.error("authentication failed, no token generated");
			}
		} catch (Exception ex) {
			// check if exception is due to Expired
			if (ex.getMessage().contains("The incoming token has expired")) {
				// Refresh Token
				logger.info("Refresh Token");
				refreshToken();
			} else {
				logger.error("Error while refreshing the token -{}", ex.getMessage());
			}
			logger.error("Error while refreshing the token, ", ex);
		}
		return token;

	}

	@Override
	public FixtureResponse getFixtureData(String token, String fixtureID) {
		FixtureResponse response = null;
		//this.token= token;

		HttpHeaders headers = getHeaders();
		headers.set("Authorization", token);
		headers.set("x-api-key", X_API_KEY);
		HttpEntity<String> jwtEntity = new HttpEntity<>(headers);
		ResponseEntity<FixtureResponse> fixtureResponse;
		try {
			fixtureResponse = restTemplate.exchange(FIXTURE_URL.replace("{}", fixtureID), HttpMethod.GET, jwtEntity, FixtureResponse.class);
			if (fixtureResponse.getStatusCode().equals(HttpStatus.OK)) {
				response = fixtureResponse.getBody();
			}
		} catch (RestClientException e) {
			if(e.getMessage()!=null&&e.getMessage().contains("The incoming token has expired")) {
				logger.error("Expired Token -{}", e.getMessage());
				token= getAuthToken();
				getFixtureData(token,fixtureID); 
			} else {
				logger.error("Error while fetching Fixture Data -{}", e.getMessage());
			}
		} catch (Exception e) {
			logger.error("Error while fetching Fixture Data -{}", e.getMessage());
		}
		return response;
	}

	private void refreshToken() {
		User authenticationUser = User.builder().user(USERNAME).password(PWD).refreshtoken(token).build();

		String authenticationBody;
		try {
			authenticationBody = new ObjectMapper().writeValueAsString(authenticationUser);
			HttpHeaders authenticationHeaders = getHeaders();
			HttpEntity<String> authenticationEntity = new HttpEntity<>(authenticationBody, authenticationHeaders);
			ResponseEntity<ResponseToken> refreshTokenResponse = restTemplate.exchange(REFRESH_TOKEN, HttpMethod.POST,
					authenticationEntity, ResponseToken.class);
			if (refreshTokenResponse.getStatusCode().equals(HttpStatus.OK)&&refreshTokenResponse.getBody()!=null) {
				token = refreshTokenResponse.getBody().getIdToken();
			}else {
				logger.error("refresh failed, no token generated");
			}
		} catch (JsonProcessingException e) {
			logger.error("Error on refreshing the token -{}", e.getMessage());
		}
	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}

	@Override
	public void updateScore(EventRotation eventRotation, ScoreDetails scoreParameters) {

		// ScoreUpdate object update from Hessian call and sending to downstream
		// ScoringUpdate application
		ScoreUpdate scoreUpdate = generateScoreUpdate(scoreParameters.getLeagueId(),
									eventRotation, scoreParameters.getAwayScore(), scoreParameters.getHomeScore(),
									scoreParameters.getPeriod(), scoreParameters.getTimer(), scoreParameters.isPeriodEndFlag(),
									scoreParameters.isHalftimeFlag(), scoreParameters.isFinalFlag(), scoreParameters.getSituationText());

		sendScoreUpdateMessage(scoreParameters.getJmsDestination(), scoreUpdate);
		scoreUpdateCache.put(scoreUpdate);
	}

	@Override
	public void updateMatchupScore(EventRotation eventRotation, ScoreDetails scoreParameters) {
		BBMatchupScore matchupScore = generateBBMatchupScoreUpdate(eventRotation, scoreParameters.getHomeTeamName(), scoreParameters.getAwayTeamName(), scoreParameters.getHomeScore(), scoreParameters.getAwayScore(),
																   scoreParameters.getPeriod(), scoreParameters.getPeriodName(), scoreParameters.getTimer(), scoreParameters.getPossession(), scoreParameters.getFreeThrowsRemaining(), scoreParameters.isFinalFlag(), scoreParameters.isClockRunning());
		sendMatchupScoreUpdateMessage(scoreParameters.getMsJmsDestination(), matchupScore);
	}

	public BBMatchupScore generateBBMatchupScoreUpdate(EventRotation eventRotation, String homeTeam, String awayTeam,  String homeScore, String awayScore,
													   Integer periodId, String period, String timer, String possession, int freeThrown, boolean finalFlag, boolean isClockRunning) {
		BBMatchupScore matchupScore = new BBMatchupScore();
		matchupScore.setEventId(eventRotation.getEventId());
		matchupScore.setAwayRotation(eventRotation.getAwayRot());
		matchupScore.setHomeRotation(eventRotation.getAwayRot() + 1);
		matchupScore.setSequence(periodId);
		matchupScore.setPeriod(period);
		matchupScore.setIsFinal(finalFlag ? "true" : "false");
		matchupScore.setDescription(timer);
		matchupScore.setPause(isClockRunning?0:1);
		setScores(matchupScore, awayScore, homeScore, eventRotation.isFlip());
		setPossessionAndFreeThrown(matchupScore, possession, homeTeam, awayTeam, freeThrown, eventRotation.isFlip());

		return matchupScore;
	}

	private void setScores(BBMatchupScore matchupScore, String awayScore, String homeScore, boolean isFlip) {
		matchupScore.setAwayScore(isFlip?homeScore:awayScore);
		matchupScore.setHomeScore(isFlip?awayScore:homeScore);
	}

	private void setPossessionAndFreeThrown(BBMatchupScore matchupScore, String possession, String homeTeam, String awayTeam, int freeThrown, boolean isFlip) {
		if(possession.equals(PossessionTeamSide.HOME.value())) {
			matchupScore.setPossession(isFlip?2:1);
			matchupScore.setPossessionTeam(isFlip?awayTeam:homeTeam);

		}else if(possession.equals(PossessionTeamSide.AWAY.value())) {
			matchupScore.setPossession(isFlip?1:2);
			matchupScore.setPossessionTeam(isFlip?homeTeam:awayTeam);
		}else {
			matchupScore.setPossession(0);
			matchupScore.setPossessionTeam("none");
		}
		matchupScore.setFreeThrow(freeThrown>0?"True":"False");
		matchupScore.setHomeFT(isFlip?0:freeThrown);
		matchupScore.setAwayFT(isFlip?freeThrown:0);
	}

	public ScoreUpdate generateScoreUpdate(Integer leagueId, EventRotation eventRotation,
										   String awayScore, String homeScore, Integer period, String timer, boolean periodEndFlag,
										   boolean halftimeFlag, boolean finalFlag, String situationText) {

		Score away = new Score();
		away.setRotNum(eventRotation.getAwayRot());

		Score home = new Score();
		home.setRotNum(eventRotation.getAwayRot() + 1);

		if (eventRotation.isFlip()) {
			away.setScoreText(homeScore);
			home.setScoreText(awayScore);
		} else {
			away.setScoreText(awayScore);
			home.setScoreText(homeScore);
		}

		List<Score> scoreList = new ArrayList<>();
		scoreList.add(away);
		scoreList.add(home);

		ScoreUpdate sr = new ScoreUpdate();
		sr.setEventId(eventRotation.getEventId());
		sr.setLeagueId(leagueId);
		sr.setPeriod(period);
		sr.setTime(timer);
		sr.setHalfTime(halftimeFlag ? "true" : "false");
		sr.setFinalScore(finalFlag ? "true" : "false");
		sr.setCorrection("false");
		sr.setPeriodEnd(periodEndFlag ? "true" : "false");
		sr.setMessageSource(scoreMsgSource);
		sr.setSituationText(situationText);

		sr.setScoreList(scoreList);

		return sr;
	}

	public String buildScoreXML(Serializable scoreUpdate) {
		StringWriter writer = new StringWriter();
		Serializer serializer = new Persister();

		try {
			serializer.write(scoreUpdate, writer);
		} catch (Exception e) {
			logger.error("buildScoreXML, ", e);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append('\n');
		sb.append(writer);

		logger.info("======== Row Score============");
		logger.info(sb.toString());

		return sb.toString();
	}

	public void sendScoreUpdateMessage(String topic, ScoreUpdate scoreUpdate) {
		if (scoreUpdate == null) {
			return;
		}

		//Build ScoreXml update
		String scoreXml = buildScoreXML(scoreUpdate);

		//Get ScoreXml from ScoreXMLCache
		String existXml = scoreXmlCache.get(scoreUpdate.getEventId());

		if (existXml != null && existXml.equals(scoreXml)) {
			logger.info("Not sending score msg since the same score xml was sent. eventId={}", scoreUpdate.getEventId());
			return;
		}

		boolean success = sendMessageService.send(topic, scoreXml, false);
		if (success) {
			logger.info("Score msg sent successfully, eventId={}", scoreUpdate.getEventId());
			scoreXmlCache.put(scoreUpdate.getEventId(),scoreXml);
		} else {
			logger.info("Score msg sending failure, eventId={}", scoreUpdate.getEventId());
		}
	}

	public void sendMatchupScoreUpdateMessage(String topic, BBMatchupScore scoreUpdate) {
		if (scoreUpdate == null) {
			return;
		}

		//Build ScoreXml update
		String scoreXml = buildScoreXML(scoreUpdate);

		//Get ScoreXml from ScoreXMLCache
		String existXml = matchupScoreXMLCache.get(scoreUpdate.getEventId());

		if (existXml != null && existXml.equals(scoreXml)) {
			logger.info("Not sending matchup score msg since the same matchup score xml was sent. eventId={}", scoreUpdate.getEventId());
			return;
		}

		boolean success = sendMessageService.send(topic, scoreXml, true);
		if (success) {
			logger.info("Matchup score msg sent successfully, eventId={}", scoreUpdate.getEventId());
			matchupScoreXMLCache.put(scoreUpdate.getEventId(), scoreXml);
		} else {
			logger.info("Matchup score msg sending failure, eventId={}", scoreUpdate.getEventId());
		}
	}


}
