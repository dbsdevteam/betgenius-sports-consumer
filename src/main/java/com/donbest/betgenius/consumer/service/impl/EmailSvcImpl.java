package com.donbest.betgenius.consumer.service.impl;

import java.util.Date;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.donbest.betgenius.consumer.service.EmailSvc;

@Service("EmailServiceImpl")
public class EmailSvcImpl implements EmailSvc {
	
	private final JavaMailSender mailSender;

	public EmailSvcImpl(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Override
	public void sendEmail(String from, String[] to, String[] cc, String subject, String text) {

		try {
			Date date = new Date();
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setFrom(from);
			mailMessage.setTo(to);
			mailMessage.setCc(cc);
			mailMessage.setSubject(subject);
			mailMessage.setText(text);
			mailMessage.setSentDate(date);
			mailSender.send(mailMessage);

		} catch (MailException e) {
			e.printStackTrace();
		}
	}
}