package com.donbest.betgenius.consumer.service;

import org.springframework.stereotype.Service;

@Service
public interface HeartbeatService {

    public String heartbeat();

}
