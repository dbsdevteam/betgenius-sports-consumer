package com.donbest.betgenius.consumer.service.impl;

import com.donbest.betgenius.consumer.cache.EventRotationCache;
import com.donbest.betgenius.consumer.cache.EventRotationDBCache;
import com.donbest.betgenius.consumer.cache.FixtureCache;
import com.donbest.betgenius.consumer.cache.RotationMailCache;
import com.donbest.betgenius.consumer.configuration.EmailConfig;
import com.donbest.betgenius.consumer.configuration.RotationAdjuster;
import com.donbest.betgenius.consumer.dao.HattrickBetgeniusDao;
import com.donbest.betgenius.consumer.fixtures.model.FixtureResponse;
import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.consumer.service.BetGeniusFixtureService;
import com.donbest.betgenius.consumer.service.BetGeniusService;
import com.donbest.betgenius.consumer.service.EmailSvc;
import com.donbest.betgenius.consumer.utils.Utils;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.donbest.betgenius.hattrick.service.BridgeService;
import com.sportsdataservices.schemas.integrationservice.v3.Competitor;
import com.sportsdataservices.schemas.integrationservice.v3.Fixture;
import com.sportsdataservices.schemas.integrationservice.v3.HomeAway;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class BetGeniusFixtureServiceImpl implements BetGeniusFixtureService {

	private static final Logger logger = LoggerFactory.getLogger(BetGeniusFixtureServiceImpl.class);

	final
	EmailSvc emailService;

	final
	HattrickBetgeniusDao betgeniusDao;

	final
	EventRotationDBCache eventRotDBCache;

	final
	RotationAdjuster rotationAdjuster;

	final
	BetGeniusService betGeniusService;

	final
	BridgeService bridgeService;

	final
	RotationMailCache rotMailCache;

	final
	FixtureCache fixtureCache;

	final
	EventRotationCache eventRotCache;

	final
	EmailConfig emailConfig;

	private final ISO8601DateConverter iso8601DateConverter = new ISO8601DateConverter();

	public BetGeniusFixtureServiceImpl(HattrickBetgeniusDao betgeniusDao, EventRotationDBCache eventRotDBCache, RotationAdjuster rotationAdjuster, BetGeniusService betGeniusService, BridgeService bridgeService, RotationMailCache rotMailCache, EmailSvc emailService, FixtureCache fixtureCache, EventRotationCache eventRotCache, EmailConfig emailConfig) {
		this.betgeniusDao = betgeniusDao;
		this.eventRotDBCache = eventRotDBCache;
		this.rotationAdjuster = rotationAdjuster;
		this.betGeniusService = betGeniusService;
		this.bridgeService = bridgeService;
		this.rotMailCache = rotMailCache;
		this.emailService = emailService;
		this.fixtureCache = fixtureCache;
		this.eventRotCache = eventRotCache;
		this.emailConfig = emailConfig;
	}

	private boolean isValidEventRotation(EventRotation eventRotation) {
		return  eventRotation!=null&&eventRotation.getAwayRot()!=null&&eventRotation.getAwayRot()>0;
	}

	@Override
	public EventRotation updateFixture(String fixtureID, Fixture fixture) {

		EventRotation currentFixture = fixtureCache.get(fixtureID);
		logger.debug("currentFixture, {}", currentFixture);
		if(fixture!=null) {
			currentFixture = getEventRotationFromFixture(fixture);
			logger.debug("getEventRotationFromFixture, {}", currentFixture);
		}else if(currentFixture==null){
			currentFixture = getEventRotationFromFixtureID(fixtureID);
			logger.debug("getEventRotationFromFixtureID, {}", currentFixture);
		}
		if(currentFixture!=null) {
			fixtureCache.put(fixtureID, currentFixture);
		}else {
			logger.error("can't find fixture, fixtureId={}", fixtureID);
			return null;
		}
		League league = League.valueOfId(currentFixture.getLeagueId());
		EventRotation eventRot = eventRotCache.get(fixtureID);
		logger.debug("currentEventRotation, {}", eventRot);
		if(!isMatchEventRotFixture(eventRot, currentFixture)) {
			eventRot = getEventRotation(league, fixtureID, 3, currentFixture);
			logger.debug("getEventRotation, {}", eventRot);
		}

		//save eventRot
		// Insert/Updating the EventRotation in Local Database
		Date startDate = new Date(currentFixture.getStartTime());
		logger.debug("insert update, startDate:{}", startDate);
		insertUpdateEventRotDB(fixtureID, currentFixture.getAwayTeam(),	currentFixture.getHomeTeam(), league, startDate, eventRot);

		if(eventRot!=null) {
			eventRotCache.put(fixtureID, eventRot);
		}
		return eventRot;
	}

	private boolean isMatchNonEventRotFixture(EventRotation currentFixture, String awayTeam, String homeTeam, Date startDate) {
		if(currentFixture!=null) {
			return  currentFixture.getAwayTeam().equals(awayTeam)&&
					currentFixture.getHomeTeam().equals(homeTeam)&&
					currentFixture.getStartTime().equals(startDate.getTime());
		}
		return false;
	}

	private boolean isMatchEventRotFixture(EventRotation eventRot, EventRotation currentFixture) {
		if(eventRot!=null&&currentFixture!=null) {
			return  currentFixture.getHomeTeam().equals(eventRot.getHomeTeam())&&
					currentFixture.getAwayTeam().equals(eventRot.getAwayTeam())&&
					currentFixture.getStartTime().equals(eventRot.getStartTime());
		}
		return false;
	}

	private boolean isMatchEventRot(EventRotation eventRot, EventRotation currentFixture) {
		if(eventRot!=null&&currentFixture!=null) {
			return  currentFixture.getHomeTeam().equals(eventRot.getHomeTeam())&&
					currentFixture.getAwayTeam().equals(eventRot.getAwayTeam())&&
					currentFixture.getStartTime().equals(eventRot.getStartTime())&&
					currentFixture.getAwayRot().equals(eventRot.getAwayRot());
		}
		return false;
	}

	public EventRotation getEventRotationFromFixture(Fixture fixture) {
		// creat new fixture
		EventRotation eventRotation = new EventRotation();
		Competitor competitor1 = fixture.getCompetitors().getCompetitors().get(0);
		Competitor competitor2 = fixture.getCompetitors().getCompetitors().get(1);
		String team1 = replaceNonEnglishCharacters(competitor1.getName());
		String team2 = replaceNonEnglishCharacters(competitor2.getName());
		League league = getLeague(fixture.getSport().getName(), fixture.getCompetition().getName());
		eventRotation.setLeagueId(league.getId());
		if(competitor1.getHomeAway()==HomeAway.HOME) {
			eventRotation.setHomeTeam(team1);
			eventRotation.setAwayTeam(team2);
		}else {
			eventRotation.setAwayTeam(team1);
			eventRotation.setHomeTeam(team2);
		}
		eventRotation.setStartTime(fixture.getStartTimeUtc().toGregorianCalendar().getTime().getTime());
		return eventRotation;
	}

	public EventRotation getEventRotationFromFixtureID(String fixtureId) {
		logger.info("Invoking Fixture Api, fixtureId={}", fixtureId);

		String token = betGeniusService.getAuthToken();
		FixtureResponse fixtureResponse = betGeniusService.getFixtureData(token, fixtureId);

		logger.info("Fixture API response: {}", ReflectionToStringBuilder.toString(fixtureResponse, ToStringStyle.JSON_STYLE, true, true));
		// creat new fixture
		EventRotation eventRotation = new EventRotation();
		com.donbest.betgenius.consumer.fixtures.model.Fixture fixture = fixtureResponse.get_embedded().getFixtures().get(0);
		League league = getLeague(fixture.getSportName(), fixture.getCompetitionName());
		eventRotation.setLeagueId(league.getId());
		String team1 = replaceNonEnglishCharacters(fixture.getFixturecompetitors().get(1).getCompetitor().getName());
		String team2 = replaceNonEnglishCharacters(fixture.getFixturecompetitors().get(0).getCompetitor().getName());
		eventRotation.setAwayTeam(team1);
		eventRotation.setHomeTeam(team2);
		String startDateString = fixture.startDate;
		Date startDate = Utils.convertDateStringToDate(startDateString);
		if(startDate==null) {
			logger.error("fixture converting start date error, {}", startDateString);
			return null;
		}else {
			eventRotation.setStartTime(startDate.getTime());
		}
		return eventRotation;
	}

	public EventRotation getEventRotation(League league, String fixtureID,  Integer hourGap, EventRotation fixtureEventRotation) {
		logger.debug("start getEventRotation, Event:{}", fixtureEventRotation);

		//Date scheduleOpenTime = getDateFromIso8601String(scheduleOpenTimeString);
		SimpleDateFormat sdfDateAndTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

		String groupName = "(dbLeague=" + league.getId() + ")";
		String msgKey = groupName + "_" + fixtureID;
		EventRotation eventRot = null;

		try {
			eventRot = findEventRotationFromDatabase(fixtureID, fixtureEventRotation);
			if (eventRot!=null){
				eventRotDBCache.put(fixtureID, eventRot);
				logger.debug("Found Event Rotation from Database, Event:{}", eventRot);
			}else {
				eventRotDBCache.evictEventRot(fixtureID);
				logger.debug("Remove Event Rotation from Cache, fixtureID:{}", fixtureID);
			}

			if(isValidEventRotation(eventRot)) {
				logger.debug("Found Valid of Event Rotation from Database, Event:{}", eventRot);
				return eventRot;
			}

			if(fixtureEventRotation!=null) {
				Date startDate = new Date(fixtureEventRotation.getStartTime());
				String awayName = fixtureEventRotation.getAwayTeam();
				String homeName = fixtureEventRotation.getHomeTeam();
				eventRot = bridgeService.getEventRotationByLeaugeIdAndTeamNameAndGameDateHourGap(league.getId(), awayName, homeName, startDate, hourGap);
				logger.debug("Looking Value of Event Rotation from Bridge Service, {}, {}, {}, {}, {}", league.getId(), awayName, homeName, startDate, hourGap);
				if(eventRot==null&&league==League.WNCAAB) {
					//specific for WNCAAB, remove women in the team tame
					awayName = awayName.toLowerCase().replace("women", "").trim();
					homeName = homeName.toLowerCase().replace("women", "").trim();
					eventRot = bridgeService.getEventRotationByLeaugeIdAndTeamNameAndGameDateHourGap(league.getId(), awayName, homeName, startDate, hourGap);
					logger.debug("Looking Value of WNCAAB Event Rotation from Bridge Service, {}, {}, {}, {}, {}", league.getId(), awayName, homeName, startDate, hourGap);
				}
				if (eventRot != null) {
					eventRot.setStartTime(startDate.getTime());
					eventRot.setAwayTeam(fixtureEventRotation.getAwayTeam());
					eventRot.setHomeTeam(fixtureEventRotation.getHomeTeam());
					eventRot.setLeagueId(league.getId());
					logger.debug("Found Value of Event Rotation from Bridge Service, Event:{}", eventRot);

					// sometimes returns weird eventRot, double check
					if (eventRot.getAwayRot()!=null&&eventRot.getAwayRot()>0&&eventRot.getEventId()!=null&&eventRot.getEventId()>0) {
						// check if it is in-game rotation, and adjust by offset if required.
						// adjusters are configured in the application.yml file.
						rotationAdjuster.adjust(eventRot);
						rotMailCache.evictMsgKey(msgKey);
					} else {
						eventRot = null;
					}
				}
				//send email alert if null
				if (eventRot==null) {
					logger.debug("EventRotation returns null, fixture:{}", fixtureEventRotation);
					logger.debug("EventRotation not present in donbest schedule");
					String mailMsg = groupName + "\nTime=" + sdfDateAndTime.format(startDate) + ", Away=" + fixtureEventRotation.getAwayTeam() + ", Home="	+ fixtureEventRotation.getHomeTeam();
					sendNoRotationMail(msgKey, mailMsg, emailService);
				}
			}
		} catch (Exception e) {
			logger.error("Catch exception in RotationLookup: ", e);
		}

		return eventRot;
	}

	public Date getDateFromIso8601String(String dateString) {
		return (Date) iso8601DateConverter.fromString(dateString);
	}

	public EventRotation findEventRotationFromDatabase(String matchId, EventRotation fixtureRotation) {
		EventRotation eventRotation;
		eventRotation = betgeniusDao.findEventRotationFromMatchId(matchId);
		if (eventRotation!=null && fixtureRotation!=null) {
			logger.debug("Event Rotation from DB details, {}", eventRotation);
			logger.debug("fixtureRotation details, {}", fixtureRotation);
			if(isMatchEventRotFixture(eventRotation, fixtureRotation)) {
				return eventRotation;
			}else {
				logger.debug("Mismatch with Team names performing DB delete");
				int rowsAffected = betgeniusDao.deleteMatchIdFromDatabase(matchId);
				logger.info("Match ID :[" + matchId + "] deleted from Betgenius Table. No. of rows affected : ["	+ rowsAffected + "]");
				return null;
			}
		}
		return eventRotation;
	}

	protected void sendNoRotationMail(String msgKey, String msg, EmailSvc emailService) {
		logger.debug("To send no rotation email, msgKey:{}", msgKey);
		if(msgKey == null) {
			return;
		}

		if(rotMailCache.get(msgKey) == null) {
			String title = emailConfig.getSubject();
			String strStart = emailConfig.getMessagePrefix();
			String mailMsgSent = strStart + msg;
			String emailFrom = emailConfig.getFrom();
			String[] emailTo = StringUtils.split(emailConfig.getTo(), ";,");
			String[] emailCc = StringUtils.split(emailConfig.getCc(), ";,");
			emailService.sendEmail(emailFrom, emailTo, emailCc, title, mailMsgSent);
			logger.debug("Sending no rotation email, msgKey:{}", msgKey);
			rotMailCache.put(msgKey,"MailSent");
		}

	}

	@Override
	public void insertUpdateEventRotDB(String matchId, String awayTeamName, String homeTeamName, League league, Date scheduled_time, EventRotation eventRotation) {
		logger.debug("insertUpdateEventRotDB, matchId:{}, away:{}, home:{}, scheduled_time:{}, eventRotation:{}", matchId, awayTeamName, homeTeamName,  scheduled_time.getTime(), eventRotation);

		EventRotation existEventRotation;
		existEventRotation = eventRotDBCache.get(matchId);

		if(existEventRotation != null) {
			logger.debug("Event Rotation value from DBCache, {}", existEventRotation);
		}

		try {
			EventRotation result = new EventRotation();
			int rowsAffectedInsert = 0;
			if (!isValidEventRotation(eventRotation)) {
				if(isMatchNonEventRotFixture(existEventRotation, awayTeamName, homeTeamName, scheduled_time)) {
					logger.debug("Equals non eventRotation, ignore updating");
				}else {
					result.setHomeTeam(homeTeamName);
					result.setAwayTeam(awayTeamName);
					result.setStartTime(scheduled_time.getTime());
					result.setEventId(league.getId());
					rowsAffectedInsert = betgeniusDao.insertOrUpdateMatchIdWhenCountZeroAndEventRotNull(matchId, awayTeamName, homeTeamName, league, scheduled_time, new Date());
					logger.debug("insertOrUpdateMatchIdWhenCountZeroAndEventRotNull, number:{} {} {} {} {} {} {} {}", rowsAffectedInsert, matchId, awayTeamName, homeTeamName, league.getId(), league.getSport(), league.getCategory(), scheduled_time);
				}
			} else if (isMatchEventRot(eventRotation, existEventRotation)) {
				logger.debug("Equals eventRotation, ignore updating");
			} else {
				result = eventRotation;
				rowsAffectedInsert = betgeniusDao.insertOrUpdateMatchIdWhenCountZeroAndEventRotNotNull(matchId, awayTeamName, homeTeamName, league, scheduled_time, eventRotation, new Date());
				logger.debug("insertOrUpdateMatchIdWhenCountZeroAndEventRotNotNull, {}", eventRotation);
			}
			if (rowsAffectedInsert > 0) {
				logger.debug("DB Insert/Update rows affected:{}, MatchId ={}, AwayTeam={}, HomeTeam={}, League={}, Sport={}, Category={}, eventRot={}", rowsAffectedInsert, matchId, awayTeamName, homeTeamName, league.getId(), league.getSport(), league.getCategory(), result);
				eventRotDBCache.put(matchId, result);
			}
		} catch (Exception e) {
			logger.error("insertUpdateEventRotDB failed, ", e);
			e.printStackTrace();
		}
	}

	protected String replaceNonEnglishCharacters(String name) {
		String temp = replaceNonEnglishCharacterLocal(name);
		return Normalizer.normalize(temp, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	protected String replaceNonEnglishCharacterLocal(String name) {
		if (name != null) {
			name = name.replace("á", "a").replace("ä", "a").replace("å", "a").replace("ã", "a").replace("Ã", "A")
					.replace("Ä", "A").replace("Å", "A").replace("Á", "A");
			name = name.replace("ć", "c").replace("ç", "c");
			name = name.replace("é", "e").replace("è", "e").replace("ę", "e");
			name = name.replace("ğ", "g");
			name = name.replace("ı", "i").replace("í", "i").replace("İ", "I");
			name = name.replace("ł", "l").replace("Ł", "L");
			name = name.replace("ñ", "n").replace("Ñ", "N");
			name = name.replace("ó", "o").replace("ø", "o").replace("ö", "o").replace("Ö", "O");
			name = name.replace("ř", "r");
			name = name.replace("ş", "s");
			name = name.replace("ú", "u").replace("ü", "u").replace("ù", "u").replace("Ü", "U").replace("ů", "u");
			name = name.replace("ź", "z");
		}

		return name;
	}

	private League getLeague(String sports, String competition) {
		//Todo current hardcode
		if(competition.toLowerCase().contains("women")) {
			return League.WNCAAB;
		}else {
			return League.NCAAB;
		}
	}
}