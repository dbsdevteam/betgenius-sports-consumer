package com.donbest.betgenius.consumer.service;

import com.donbest.betgenius.consumer.fixtures.model.FixtureResponse;
import com.donbest.betgenius.consumer.model.common.ScoreDetails;
import com.donbest.betgenius.hattrick.model.EventRotation;

public interface BetGeniusService {

	String getAuthToken();

	FixtureResponse getFixtureData(String token,String fixtureID);

	void updateScore(EventRotation eventRotation, ScoreDetails scoreParameters);
	void updateMatchupScore(EventRotation eventRotation, ScoreDetails scoreParameters);

}
