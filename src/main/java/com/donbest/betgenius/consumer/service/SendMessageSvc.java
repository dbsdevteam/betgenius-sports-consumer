package com.donbest.betgenius.consumer.service;

public interface SendMessageSvc {

	public boolean send(String destinationName, String message, boolean pubSubDomain);

}