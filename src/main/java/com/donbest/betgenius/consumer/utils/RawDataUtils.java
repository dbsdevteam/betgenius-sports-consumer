package com.donbest.betgenius.consumer.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

@Service
public class RawDataUtils {

    private static final Logger log = LoggerFactory.getLogger(RawDataUtils.class);

    private final RawDataConfig config;

    @Autowired
    public RawDataUtils(RawDataConfig config) {
        this.config = config;
    }

    public boolean sendRawDataToXmlFile(String sportsbookName, String rawDataParameter, String todayString, String puncherTimeStamp, String extraSportLeagueInfo, String subFolder)
    {
        if (!config.isSaveXmlFiles()) return false;

        if(rawDataParameter == null) return false;

        try
        {
            String folder = config.getFolderPath();
            boolean isWindows = false;
            String osName = System.getProperty("os.name");
            // Linux / Windows Server 2016 / Mac OS
//			log.info("os.name=" + osName);
            if(osName != null && osName.toUpperCase().contains("WINDOWS")) {
                isWindows = true;
                folder = config.getFolderWindows();
            }

            log.debug("Folder = {}", folder);

            // create folder if not exist
            new File(folder).mkdir();

            // delete old folder
//			if(deleteDateString != null)
            // not delete local folder
            if(folder.contains(config.getFolderRoot()) || isWindows)
            {

                Date deleteDate = DateUtils.addDays(new Date(), -config.getKeepSourceDays());	// keep # days data

                // Search previous 2 * # days and delete them
                for(int i = 1 ; i < (2 * config.getKeepSourceDays()) ; i++)
                {
                    Date tempDeleteDate = DateUtils.addDays(deleteDate, -i);

                    String deleteDateString1 = DateFormatUtils.format(tempDeleteDate, "yyyyMMdd");

                    String deleteFolder = folder + deleteDateString1;
//					log.info("deleteFolder = {}", deleteFolder);
                    File f = new File(deleteFolder);
                    if(f.exists())
                    {
                        log.info("Folder {} exist. Deleting now.", deleteFolder);
                        FileUtils.deleteDirectory(f);
                    }
                }
            }

            // create new folder
            String todayFolder = folder + todayString + "/";
            new File(todayFolder).mkdir();

            String sportsbookFolder = todayFolder + sportsbookName + "/";
            new File(sportsbookFolder).mkdir();

            if(subFolder != null) {
                sportsbookFolder = sportsbookFolder + subFolder.replace("/", "_") + "/";
                new File(sportsbookFolder).mkdir();
            }

            // convert to utf-8
//			String charset = "UTF-8";
            byte[] bytes = rawDataParameter.getBytes();
//			String rawDataUTF8 = new String(bytes, charset);

            ByteArrayOutputStream byteStream = new ByteArrayOutputStream(bytes.length);
            try {
                try (GZIPOutputStream zipStream = new GZIPOutputStream(byteStream)) {
                    zipStream.write(bytes);
                }
            } finally {
                byteStream.close();
            }

            byte[] compressedData = byteStream.toByteArray();

            String fileName = sportsbookFolder + sportsbookName + "_" + puncherTimeStamp;

            if(extraSportLeagueInfo != null)
                fileName = fileName + "_" + extraSportLeagueInfo.replace("/", "_");

            fileName = fileName + ".xml.gz";
			log.debug("FileName = {}", fileName);

            FileOutputStream fileStream = new FileOutputStream(fileName);

            try {
                fileStream.write(compressedData);
            } finally {
                try {
                    fileStream.close();
                } catch (Exception e) {
                    log.error("sendRawDataToXmlFile: {}", e, e);
                }
            }

			/*
			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(rawDataUTF8);
//			bw.newLine();
			bw.close();
			*/
            return true;

        } catch (Exception e) {
            log.error("sendRawDataToXmlFile: {}", e, e);
//			e.printStackTrace();
//			throw e;
//			sendMail(sportsbookName, (new Date()).toString(), "sendRawDataToXmlFile error.\n" + e.toString(), true);
            return false;
        }
    }
}
