package com.donbest.betgenius.consumer.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class RawDataConfig {

    @Value("${raw.data.save-xml-files}")
    protected boolean saveXmlFiles;

    @Value("${raw.data.folder.path}")
    protected String folderPath;

    @Value("${raw.data.folder.root}")
    protected String folderRoot;

    @Value("${raw.data.folder.windows}")
    protected String folderWindows;

    @Value("${raw.data.keep-source-days}")
    protected int keepSourceDays;

}
