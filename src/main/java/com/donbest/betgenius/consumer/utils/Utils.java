package com.donbest.betgenius.consumer.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Utils {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
    private static final DateFormat DATE_FORMAT_STRING = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static <J> String printXml(final J instance) throws JAXBException {
        return printXml(instance, instance.getClass());
    }

    public static <J> String printXml(final J instance,
                                      final Class<?>... classesToBeBound)
            throws JAXBException {

        final JAXBContext context = JAXBContext.newInstance(classesToBeBound);

        final ByteArrayOutputStream output = new ByteArrayOutputStream();

        final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.marshal(instance, output);
        /* output.flush(); // Nasty IOException */
        final String jaxbEncoding = (String) marshaller.getProperty(Marshaller.JAXB_ENCODING);

        try {
            return output.toString(jaxbEncoding);
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(uee);
        }
    }

    public static String convertXMLGregorianCalendarToDateString(XMLGregorianCalendar calendar) {
        Date date = calendar.toGregorianCalendar().getTime();
        return DATE_FORMAT.format(date);
    }

    public static String convertLongToDate(long time) {
        Date date = new Date(time);
        return DATE_FORMAT.format(date);
    }

    public static Date convertDateStringToDate(String dateString) {
        try {
            DATE_FORMAT_STRING.setTimeZone(TimeZone.getTimeZone("UTC"));
            return DATE_FORMAT_STRING.parse(dateString);
        }catch (Exception ex) {
            return null;
        }
    }

    public static String getRemainingTimesFromMilliseconds(int milliseconds) {
        int remainSeconds = milliseconds / 1000;
        int minutes = remainSeconds / 60;
        int seconds = remainSeconds % 60;
        return String.format("%02d:%02d", minutes, seconds);
    }

}
