package com.donbest.betgenius.consumer.handlers;

import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.consumer.utils.Utils;
import com.sportsdataservices.schemas.basketball.matchstate.external.v2.*;
import com.sportsdataservices.schemas.integrationservice.v3.Header;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.donbest.betgenius.consumer.model.common.ScoreDetails;
import com.donbest.betgenius.hattrick.model.EventRotation;

import javax.xml.datatype.XMLGregorianCalendar;

@Component
public class NCAABHandler extends BaseHandler {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected static final String TIMER = "-NOTSET-";

    @Value("${betgenius.ncaab.enable}")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Value("${betgenius.ncaab.destination}")
    private String ncaabTopic;

    @Value("${betgenius.ncaab.msDestination}")
    private String matchupScoreTopic;

    @Value("${betgenius.ncaab.setPeriodEnd}")
    private boolean setPeriodEnd;

    @Value("${betgenius.ncaab.setHalftime}")
    private boolean setHalftime;

    @Value("${betgenius.ncaab.setFinal}")
    private boolean setFinal;

    @Value("${betgenius.ncaab.matchupScore}")
    private boolean isMatchupScore;

    @Override
	public ScoreDetails prepareForUpdate(League league, EventRotation eventRotation, Updategram updategram) {

        ScoreDetails scoreDetails = new ScoreDetails();
        scoreDetails.setLeagueId(league.getId());
        scoreDetails.setTimer(TIMER);
        scoreDetails.setScheduleDateString(Utils.convertLongToDate(eventRotation.getStartTime()));
        scoreDetails.setHomeTeamName(eventRotation.getHomeTeam());
        scoreDetails.setAwayTeamName(eventRotation.getAwayTeam());
        scoreDetails.setSport(league.getSport());
        scoreDetails.setCategory(league.getCategory());
        scoreDetails.setJmsDestination(ncaabTopic);
        scoreDetails.setMsJmsDestination(matchupScoreTopic);

        scoreDetails.setPeriodEndFlag(false);
        scoreDetails.setHalftimeFlag(false);
        scoreDetails.setFinalFlag(false);

        BasketballMatchSummary basketballMatchSummary = updategram.getBasketballMatchSummary();
        if(basketballMatchSummary!=null) {
            scoreDetails.setEventId(String.valueOf(basketballMatchSummary.getFixtureId()));
            scoreDetails.setClockRunning(basketballMatchSummary.isIsClockRunning());
            scoreDetails.setPossession(basketballMatchSummary.getPossession().value());
            scoreDetails.setFreeThrowsRemaining(basketballMatchSummary.getNumberOfFreeThrowsRemaining());
            PhaseType phaseType = basketballMatchSummary.getCurrentPhase();
            int overtimeCount = basketballMatchSummary.getOvertimeCount();
            TimestampedTimeRemainingInPhase timestampedTimeRemainingInPhase = basketballMatchSummary.getTimeRemainingInPhaseAtTimestamp();
            if(phaseType!=null) {
                scoreDetails.setPeriod(getPeriodId(league, phaseType, overtimeCount));
                scoreDetails.setPeriodName(getPeriodName(scoreDetails.getPeriod()));
                if(isPeriodEnd(phaseType) && setPeriodEnd) {
                    scoreDetails.setPeriodEndFlag(true);
                }
                if(isHalftime(phaseType) && setHalftime) {
                    scoreDetails.setHalftimeFlag(true);
                }
                if(isFinal(phaseType) && setFinal) {
                    scoreDetails.setFinalFlag(true);
                }
            }
            if(timestampedTimeRemainingInPhase!=null) {
                scoreDetails.setTimer(Utils.getRemainingTimesFromMilliseconds(getRemainingMillionSeconds(timestampedTimeRemainingInPhase, basketballMatchSummary)));
            }
            Score score = getCurrentScore(updategram);
            if(score!=null) {
                String awayScore = String.valueOf(score.getAway());
                String homeScore = String.valueOf(score.getHome());
                if(awayScore!=null&&homeScore!=null) {
                    scoreDetails.setAwayScore(awayScore);
                    scoreDetails.setHomeScore(homeScore);
                    return scoreDetails;
                }else {
                    logger.error("missing away/home score, {}", toHeaderString(updategram.getHeader()));
                }
            }
        }else {
            logger.error("missing basketballMatchSummary, {}", toHeaderString(updategram.getHeader()));
        }
        return null;
    }


    @Override
    protected boolean canProcessMessage(League league, EventRotation eventRotation, Updategram updategram) {
        if(updategram.getBasketballMatchSummary()==null) {
            logger.info("[ {} ][ BasketballMatchSummary ][NO_PROCESS] No data for [ {} ]", league.getName(), toHeaderString(updategram.getHeader()));
            return false;
        }
        if(updategram.getBasketballMatchSummary().getScores()==null) {
            logger.info("[ {} ][ score missing ][NO_PROCESS] No data for [ {} ]", league.getName(), toHeaderString(updategram.getHeader()));
            return false;
        }
        if(updategram.getBasketballMatchSummary().getTimeRemainingInPhaseAtTimestamp()==null) {
            logger.info("[ {} ][ time remaining missing ][NO_PROCESS] No data for [ {} ]", league.getName(), toHeaderString(updategram.getHeader()));
            return false;
        }
        logger.info(constructInfoLog(league, eventRotation, updategram));
        // check new or old msg
        Integer fixtureId = updategram.getBasketballMatchSummary().getFixtureId();
        Updategram previousMsg = (Updategram) updategramCache.get(String.valueOf(fixtureId));
        if (previousMsg != null && isSame(updategram, previousMsg)) {
            logger.info("Not process, same as previous event. {}", toHeaderString(previousMsg.getHeader()));
            return false;
        }

        return true;
    }

    @Override
    protected boolean shouldUpdateScore(League league, EventRotation eventRotation, Updategram updategram) {
        if(updategram.getBasketballMatchSummary()!=null&&updategram.getBasketballMatchSummary().getCurrentPhase()!=null&&updategram.getBasketballMatchSummary().isIsReliable()) {
            return isRunning(updategram.getBasketballMatchSummary().getCurrentPhase());
        }
        return false;
    }

    @Override
    protected boolean isValidScoreDetail(ScoreDetails scoreDetails) {
        return !scoreDetails.getTimer().equals(TIMER) && scoreDetails.getPeriod()>0;
    }

    @Override
    protected boolean sendMatchupScore(League league) {
        if(league==League.NCAAB) {
            return isMatchupScore;
        }
        return false;
    }

    @Override
    public long getLastMessageTime() {
        return lastMessageTime;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private int getRemainingMillionSeconds(TimestampedTimeRemainingInPhase timestampedTimeRemainingInPhase, BasketballMatchSummary basketballMatchSummary) {
        XMLGregorianCalendar timeRemainingTimestamp = timestampedTimeRemainingInPhase.getTimeStampUtc();
        XMLGregorianCalendar currentTimestamp = basketballMatchSummary.getMessageTimestampUtc();
        boolean isClockRunning = basketballMatchSummary.isIsClockRunning();
        int diff = 0;
        if(timeRemainingTimestamp!=null&&currentTimestamp!=null) {
            diff = (int) (currentTimestamp.toGregorianCalendar().getTimeInMillis() - timeRemainingTimestamp.toGregorianCalendar().getTimeInMillis());
        }
        int remainMilliseconds = timestampedTimeRemainingInPhase.getTimeRemainingInPhaseMilliseconds();
        logger.info("getRemainingMillionSeconds remaining:{}, offset:{}, isClockRunning:{}", remainMilliseconds, diff, isClockRunning);
        if(isClockRunning&&diff>0&&remainMilliseconds>diff) {
            return remainMilliseconds - diff;
        }else {
            return remainMilliseconds;
        }
    }

    private boolean isSame(Updategram updategram, Updategram previousMsg) {
        if(updategram.getHeader()!=null&&previousMsg.getHeader()!=null) {
            return updategram.getHeader().getMessageGuid().equals(previousMsg.getHeader().getMessageGuid());
        }
        return false;
    }

    private String constructInfoLog(League league, EventRotation eventRotation, Updategram updategram) {
        Score score = getCurrentScore(updategram);
        TimestampedTimeRemainingInPhase time = updategram.getBasketballMatchSummary().getTimeRemainingInPhaseAtTimestamp();
        return "[" + league.getName() + "] [Parsed msg] FixtureId [" + updategram.getBasketballMatchSummary().getFixtureId() + "]" +
                " Scheduled [" + eventRotation.getStartTime() + "]" +
                " Away [" + eventRotation.getAwayTeam() + "] Home [" + eventRotation.getHomeTeam() + "]" +
                (score == null ? " AwayPoints [ null ] HomePoints [ null ]" : " AwayPoints [" + score.getAway() + "] HomePoints [" + score.getHome() + "]") +
                " Phase [" + updategram.getBasketballMatchSummary().getScores() + "]" +
                (time.getTimeRemainingInPhaseMilliseconds() != null ? " Clock [" + time.getTimeRemainingInPhaseMilliseconds() + "]" : " Clock [ null ]");
    }

    private boolean isFinal(PhaseType phaseType) {
        return phaseType.equals(PhaseType.POST_GAME);
    }

    private boolean isHalftime(PhaseType phaseType) {
        return phaseType.equals(PhaseType.HALF_TIME);
    }

    private boolean isPeriodEnd(PhaseType phaseType) {
        switch (phaseType) {
            case HALF_TIME:
            case BEFORE_OVERTIME:
            case POST_GAME:
                return true;
            default:
                return false;
        }
    }

    private boolean isRunning(PhaseType phaseType) {
        switch (phaseType) {
            case PRE_GAME:
            case BEFORE_SECOND_QUARTER:
            case HALF_TIME:
            case BEFORE_FOURTH_QUARTER:
            case BEFORE_OVERTIME:
            case GAME_ABANDONED:
                return false;
            default:
                return true;
        }
    }

    private String getPeriodName(int periodId) {
        switch (periodId) {
            case 1:
                return "1st Half";
            case 2:
                return "2nd Half";
            default:
                if (periodId <= 0) {
                    return "Not Set";
                } else {
                    return "OT" + (periodId - 2);
                }
        }
    }

    private int getPeriodId(League league, PhaseType phaseType, int overtimeCount) {
        //convert phase to period id
        switch (league) {
            case NCAAB:
                switch (phaseType) {
                    case PRE_GAME:
                        return 0;
                    case FIRST_QUARTER:
                    case SECOND_QUARTER:
                    case FIRST_HALF:
                        return 1;
                    case THIRD_QUARTER:
                    case FOURTH_QUARTER:
                    case SECOND_HALF:
                    case OVERTIME:
                        return 2 + overtimeCount;
                    case POST_GAME:
                    case BEFORE_SECOND_QUARTER:
                    case HALF_TIME:
                    case BEFORE_FOURTH_QUARTER:
                    case BEFORE_OVERTIME:
                    case GAME_ABANDONED:
                        return -1;
                }
                return 0;
            case WNCAAB:
                switch (phaseType) {
                    case PRE_GAME:
                        return 0;
                    case FIRST_QUARTER:
                        return 1;
                    case SECOND_QUARTER:
                    case FIRST_HALF:
                        return 2;
                    case THIRD_QUARTER:
                        return 3;
                    case FOURTH_QUARTER:
                    case SECOND_HALF:
                        return 4;
                    case OVERTIME:
                        return 4 + overtimeCount;
                    case POST_GAME:
                    case BEFORE_SECOND_QUARTER:
                    case HALF_TIME:
                    case BEFORE_FOURTH_QUARTER:
                    case BEFORE_OVERTIME:
                    case GAME_ABANDONED:
                        return -1;
                }
                return 0;
            default:
                return 0;
        }

    }

    private Score getCurrentScore(Updategram updategram) {
        if(updategram.getBasketballMatchSummary()!=null&&updategram.getBasketballMatchSummary().getScores()!=null) {
            return updategram.getBasketballMatchSummary().getScores().getCurrentScore();
        }else {
            logger.error("missing current score, {}", toHeaderString(updategram.getHeader()));
            return null;
        }
    }

    private String toHeaderString(Header header) {
        return "id:" + header.getMessageGuid() + " time:" + header.getTimeStampUtc();
    }
    
    
    

}
