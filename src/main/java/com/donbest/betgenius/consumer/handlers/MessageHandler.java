package com.donbest.betgenius.consumer.handlers;

import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;

public interface MessageHandler {

	void handle(League league, EventRotation eventRotation, Updategram updategram);
	long getLastMessageTime();
	boolean isEnabled();

}
