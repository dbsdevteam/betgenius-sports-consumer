package com.donbest.betgenius.consumer.handlers;

import com.donbest.betgenius.consumer.cache.UpdategramCache;
import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.consumer.service.BetGeniusService;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.donbest.betgenius.consumer.cache.ScoreUpdateCache;
import com.donbest.betgenius.consumer.model.common.ScoreDetails;
import com.donbest.betgenius.hattrick.model.EventRotation;

import io.micrometer.core.instrument.MeterRegistry;

public abstract class BaseHandler implements MessageHandler {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected boolean enabled = false;

	protected long lastMessageTime;

	@Autowired
	protected JmsTemplate jmsTemplate;

	@Autowired
	protected MeterRegistry registry;

	@Autowired
	protected BetGeniusService betGeniusService;
	
	@Autowired
	protected ScoreUpdateCache scoreUpdateCache;

	@Autowired
	protected UpdategramCache updategramCache;
	
	@Override
	public void handle(League league, EventRotation eventRotation, Updategram updategram) {
		lastMessageTime = System.currentTimeMillis();
		try {
			registry.counter("betgenius_message", "type", "score", "sport", league.getName()).increment();
			if(isEnabled()) {
				if (canProcessMessage(league, eventRotation, updategram)) {
					if (shouldUpdateScore(league, eventRotation, updategram)) {
						ScoreDetails scoreParams = prepareForUpdate(league, eventRotation, updategram);
						if(isValidScoreDetail(scoreParams)) {
							betGeniusService.updateScore(eventRotation, scoreParams);
							if(sendMatchupScore(league)) {
								betGeniusService.updateMatchupScore(eventRotation, scoreParams);
							}
							updategramCache.put(scoreParams.getEventId(), updategram);
						}else {
							logger.info("not valid score detail, {}", scoreParams.toString());
						}
					}
				}
			}else {
				logger.info("Handler not enabled, league={}", league);
			}
		} catch (Exception e) {
			registry.counter("betgenius_message_exception", "endpoint", league.getName()).increment();
			logger.error("Catch exception:", e);
		}
	}

	@Override
	public long getLastMessageTime() {
		return lastMessageTime;
	}

	public abstract boolean isEnabled();
	
	protected abstract ScoreDetails prepareForUpdate(League league, EventRotation eventRotation, Updategram updategram);

	protected abstract boolean canProcessMessage(League league, EventRotation eventRotation, Updategram updategram);
	
	protected abstract boolean shouldUpdateScore(League league, EventRotation eventRotation, Updategram updategram);

	protected abstract boolean isValidScoreDetail(ScoreDetails scoreDetails);

	protected abstract boolean sendMatchupScore(League league);

}
