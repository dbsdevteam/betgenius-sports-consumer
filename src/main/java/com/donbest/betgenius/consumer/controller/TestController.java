package com.donbest.betgenius.consumer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.donbest.betgenius.consumer.service.*;

@RestController
public class TestController {
	
	final
	SendMessageSvc sendMessageService;
	

	@Value("${betgenius.ncaab.destination}")
	String ncaabTopic;


	public TestController(SendMessageSvc sendMessageService, JavaMailSender mailSender) {
		this.sendMessageService = sendMessageService;
		this.mailSender = mailSender;
	}

	@GetMapping(value="/")
	public String getModel() {
		return "test";
	}

	@GetMapping(value="/sendmsg")
	public String send() {
		//jmsTemplate.convertAndSend("mlbTopic", "test joel");
		sendMessageService.send(ncaabTopic, "This is a test JMS message", false);
		return "success";
	}
	
	private final JavaMailSender mailSender;
	@GetMapping(value="/sendmail")
    public String sendMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("lakshmi.gummadi@sgdigital.com");
        message.setTo("lakshmi.gummadi@sgdigital.com","joel.delorme@sgdigital.com");
        message.setSubject("Test Email");
        message.setText("Hi, This is a test Email");
        mailSender.send(message);
        return "success";
    }
	

}
