package com.donbest.betgenius.consumer.controller;

import com.donbest.betgenius.consumer.service.HeartbeatService;
import com.donbest.betgenius.consumer.service.ProcessMessageService;
import com.donbest.betgenius.consumer.utils.RawDataUtils;
import com.donbest.betgenius.hattrick.model.EventRotation;
import com.sportsdataservices.schemas.integrationservice.v3.Updategram;
import com.sportsdataservices.schemas.integrationservice.v3.response.UpdategramResponse;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerErrorException;
import org.springframework.web.server.ServerWebInputException;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.TimeZone;

import static com.donbest.betgenius.consumer.utils.Utils.printXml;
import static org.springframework.http.MediaType.*;

@RestController
@Validated
public class TradingPlatformController {

    private static final Logger logger = LoggerFactory.getLogger(TradingPlatformController.class);

    private final HeartbeatService heartbeatService;

    private final ProcessMessageService processMessageService;

    private final RawDataUtils rawDataUtils;

    @Autowired
    public TradingPlatformController(HeartbeatService heartbeatService, ProcessMessageService processMessageService, RawDataUtils rawDataUtils) {
        this.heartbeatService = heartbeatService;
        this.processMessageService = processMessageService;
        this.rawDataUtils = rawDataUtils;
    }

    @PostMapping(path = "/heartbeat",
            consumes = ALL_VALUE,
            produces = TEXT_PLAIN_VALUE)
    public ResponseEntity<String> heartbeat() {
        logger.trace("Received heartbeat");

        String heartbeatResponse = heartbeatService.heartbeat();

        return ResponseEntity.ok().contentType(TEXT_PLAIN).cacheControl(CacheControl.noStore()).body(heartbeatResponse);
    }

    @PostMapping(path = "/processMessage",
            consumes = { TEXT_XML_VALUE, APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE },
            produces = { TEXT_XML_VALUE, APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE })
    public UpdategramResponse processMessage(@RequestBody Updategram updategram) {
        Date currentTimestamp = new Date();
        UpdategramResponse response = null;

        if (updategram != null) {
            try {
                final String updategramXMLString = printXml(updategram);
                logger.debug("Request received for processMessage: {}", updategramXMLString);
                rawDataUtils.sendRawDataToXmlFile("BetGenius", updategramXMLString,
                        DateFormatUtils.format(currentTimestamp, "yyyyMMdd"),
                        getTimestamp(updategram),
                        null, getFixtureId(updategram));
                response = processMessageService.processMessage(updategram);
                logger.debug("Response returned from processMessage: {}", printXml(response));
            } catch (JAXBException e) {
                // 500 Server Error
                logger.error("Could not print processMessage request/response as XML/JSON", e);
                ServerErrorException see;
                if (response == null) {
                    see = new ServerErrorException("Could not process XML/JSON response message", e);
                } else {
                    see = new ServerErrorException("Could not process XML/JSON request message", e);
                }
                throw see;
            }
        } else {
            // 401 Bad Request
            logger.error("Received empty input message");
            throw new ServerWebInputException("Empty input message");
        }

        return response;
    }
    
    //TODO: Temporary test endpoint - until we capture all requirements
    @PostMapping(path = "/getEventRotation", consumes = { APPLICATION_JSON_VALUE }, produces = TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getEventRotation(@RequestBody EventRotation eventRot) {
        logger.info("Fetching Event Information");
        try {
            eventRot = processMessageService.getEventRotation(eventRot.getLeagueId(), eventRot.getAwayTeam(),
                    eventRot.getHomeTeam(), new Date(eventRot.getStartTime()), 3);
        } catch (Exception e) {
            logger.error("Error fetching event from bridge service -{}", e.getMessage());
        }
        logger.info("Event Details -{}", eventRot);
        return ResponseEntity.ok().contentType(TEXT_PLAIN).cacheControl(CacheControl.noStore())
                .body(eventRot.toString());
    }

    private String getFixtureId(Updategram updategram) {
        if(updategram==null) {
            return null;
        }
        if(updategram.getCoverage()!=null) {
            return "" + updategram.getCoverage().getFixtureId();
        }
        if(updategram.getFixture()!=null) {
            return "" + updategram.getFixture().getId();
        }
        if(updategram.getBasketballMatchSummary()!=null) {
            return "" + updategram.getBasketballMatchSummary().getFixtureId();
        }
        return "0";
    }

    private String getTimestamp(Updategram updategram) {
        if(updategram!=null&&updategram.getHeader()!=null) {
            XMLGregorianCalendar timestamp = updategram.getHeader().getTimeStampUtc();
            Date date;
            if(timestamp!=null) {
                date = Date.from(updategram.getHeader().getTimeStampUtc().toGregorianCalendar().toZonedDateTime().withZoneSameInstant(TimeZone.getTimeZone("PST").toZoneId()).toInstant());
            }else {
                date = new Date();
            }
            return DateFormatUtils.format(date, "yyyyMMdd_HHmmssSSS");
        }
        return "null_" + DateFormatUtils.format(new Date(), "yyyyMMdd_HHmmssSSS");
    }
}
