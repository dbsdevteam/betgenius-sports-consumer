package com.donbest.betgenius.consumer.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Configuration
@Getter
public class EmailConfig {


    final String subject;

    final String messagePrefix;

    final String from;

    final String to;

    @Nullable
    final String cc;

    public EmailConfig(@NonNull @Value("${email.subject}") String subject,
                       @NonNull @Value("${email.message-prefix}") String messagePrefix,
                       @NonNull @Value("${email.from}") String from,
                       @NonNull @Value("${email.to}") String to,
                       @Nullable @Value("${email.cc}") String cc) {
        this.subject = subject;
        this.messagePrefix = messagePrefix;
        this.from = from;
        this.to = to;
        this.cc = cc;
    }
}
