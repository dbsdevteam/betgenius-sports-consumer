package com.donbest.betgenius.consumer.configuration;

public class RotationAdjustment {
	private Integer from;
	private Integer to;
	private Integer adjustment;
	
	public Integer getFrom() {
		return from;
	}
	
	public void setFrom(Integer from) {
		this.from = from;
	}
	
	public Integer getTo() {
		return to;
	}
	
	public void setTo(Integer to) {
		this.to = to;
	}
	
	public Integer getAdjustment() {
		return adjustment;
	}
	
	public void setAdjustment(Integer adjustment) {
		this.adjustment = adjustment;
	}
	
}
