package com.donbest.betgenius.consumer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.caucho.hessian.client.HessianProxyFactory;
import com.donbest.betgenius.hattrick.service.BridgeService;

@Configuration
public class FactoryBeanConfiguration {

	@Value("${hessian.url}")
	private String url;
	
	@Bean
	public BridgeService createHessianEndpoint() throws Exception {
        HessianProxyFactory factory = new HessianProxyFactory();
        return  (BridgeService) factory.create(BridgeService.class, url);
	}
	
}
