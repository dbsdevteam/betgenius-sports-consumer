package com.donbest.betgenius.consumer.configuration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(WebConfig.class);

    public WebConfig() {
        /* // Previously using JAX-RS
        register(JacksonJaxbJsonProvider.class);
        register(JacksonJaxbXMLProvider.class);
        register(TradingPlatformController.class);
        */
        ObjectMapper mapper = new ObjectMapper();
        //mapper.registerModule(new JacksonXmlModule());
        mapper.registerModule(new JaxbAnnotationModule());
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new StringHttpMessageConverter());
    }
    
 
}
