package com.donbest.betgenius.consumer.configuration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.donbest.betgenius.hattrick.model.EventRotation;

@Configuration
@ConfigurationProperties(prefix = "rotation-adjustments")
public class RotationAdjuster {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private List<RotationAdjustment> list;
	
	public List<RotationAdjustment> getList() {
		return list;
	}

	public void setList(List<RotationAdjustment> list) {
		this.list = list;
	}

	public void adjust(EventRotation eventRotation) {
		if (list != null) {
			Integer actualRotation = eventRotation.getAwayRot();
			for (RotationAdjustment adjustment: list) {
				if (actualRotation > adjustment.getFrom() 
						&& actualRotation < adjustment.getTo()) {
					eventRotation.setAwayRot(actualRotation + adjustment.getAdjustment());
					logger.info("Adjusting rotation number for [" + actualRotation + "] "
							+ "setting it to [" + eventRotation.getAwayRot() +"]");
					break;
				}
			}
		}
	}
	
}
