package com.donbest.betgenius.consumer.configuration;

import java.util.List;

import javax.annotation.PostConstruct;

//@Configuration
public class ApplicationProperties {

	private String test;
	
	private List<ParserDefinition> parsers;
	
	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<ParserDefinition> getParsers() {
		return parsers;
	}

	public void setParsers(List<ParserDefinition> parsers) {
		this.parsers = parsers;
	}

	@PostConstruct
	public void running() {
		System.out.println("");
	}

}
