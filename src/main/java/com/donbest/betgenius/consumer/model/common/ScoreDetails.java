package com.donbest.betgenius.consumer.model.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ScoreDetails {

	private Integer leagueId;
	private String sport;
	private String category;
	private String eventId;
	private String homeScore;
	private String awayScore;
	private Integer period;
	private String timer;
	private boolean periodEndFlag;
	private boolean halftimeFlag;
	private boolean finalFlag;
	private String scheduleDateString;
	private String homeTeamName;
	private String awayTeamName;
	private String jmsDestination;
	private String situationText;

	private boolean isClockRunning;
	private Integer freeThrowsRemaining;
	private String possession;
	private String periodName;
	private String msJmsDestination;

}
