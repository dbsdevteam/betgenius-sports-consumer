package com.donbest.betgenius.consumer.model.common;

import lombok.Getter;

@Getter
public enum League {

    NCAAB("NCAAB",      "Basketball",   "United States of America", 4),
    WNCAAB("WNCAAB",    "Basketball",   "United States of America", 17),
    UNKNOWN("Unknown",  "Unknown",      "Unknown",                  0);

    private final String name;
    private final String sport;
    private final String category;
    private final int id;

    League(String name, String sport, String category, int id) {
        this.name = name;
        this.category = category;
        this.sport = sport;
        this.id = id;
    }

    public static League valueOfId(Integer id) {
        for (League enumValue : League.class.getEnumConstants()) {
            if (enumValue.getId()==id) {
                return enumValue;
            }
        }
        return UNKNOWN;
    }

}
