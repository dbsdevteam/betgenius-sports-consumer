package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RotationMailCache {
	
	@CachePut(value = "ROTATION_MAIL", key="#msgkey", condition = "#msgkey != null")
	public String put(String msgkey, String mailMsgSent) {
		return mailMsgSent;
	}
	
	@Cacheable(cacheNames = "ROTATION_MAIL")
	public String get(String msgkey) {
		return null;
	}
	
	@CacheEvict(value = "ROTATION_MAIL", key = "#msgkey", condition = "#msgkey != null")
	public void evictMsgKey(String msgkey) {}
	
}