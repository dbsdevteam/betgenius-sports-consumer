package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpdategramCache {

    @CachePut(value = "UPDATEGRAM", key="#gameId")
    public Object put(String gameId, Object updategram) {return updategram;}

    @Cacheable(cacheNames = "UPDATEGRAM")
    public Object get(String gameId) {return null;}
}
