package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

@Configuration
@CacheConfig(cacheNames = "FIXTURE_ID")
public class FixtureIdCache {

    @Cacheable(key = "#fixtureId")
    public Integer get(Integer fixtureId) {
        return fixtureId;
    }

    @CacheEvict(key = "#ignoredFixtureId")
    public void evict(Integer ignoredFixtureId) {
    }

}
