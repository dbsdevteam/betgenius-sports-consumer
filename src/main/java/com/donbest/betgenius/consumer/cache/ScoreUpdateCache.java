package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

import com.donbest.betgenius.hattrick.model.ScoreUpdate;

@Configuration
public class ScoreUpdateCache {

	@CachePut(value = "SCORE_UPDATES", key="#scoreUpdate.eventId")
	public ScoreUpdate put(ScoreUpdate scoreUpdate) {
		return scoreUpdate;
	}
	
	@Cacheable(cacheNames = "SCORE_UPDATES")
	public ScoreUpdate get(Integer eventId) {
		return null;
	}
	
}
