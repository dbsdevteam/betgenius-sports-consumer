package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

import com.donbest.betgenius.hattrick.model.EventRotation;

@Configuration
public class EventRotationDBCache {
	
	@CachePut(cacheNames = "EVENT_ROTATION_DB_CACHE", key = "#matchId")
	public EventRotation put(String matchId, EventRotation eventRot) {
		return eventRot;
	}
	
	@Cacheable(cacheNames = "EVENT_ROTATION_DB_CACHE")
	public EventRotation get(String matchId) {
		return null;
	}
	
	@CacheEvict(cacheNames = "EVENT_ROTATION_DB_CACHE", key = "#matchId")
	public void evictEventRot(String matchId){}

}