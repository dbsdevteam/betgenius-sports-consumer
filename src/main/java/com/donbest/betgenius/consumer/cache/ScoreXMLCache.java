package com.donbest.betgenius.consumer.cache;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScoreXMLCache {
	
	@CachePut(value = "SCORE_XML", key="#eventId")
	public String put(Integer eventId, String scoreXml) {
		return scoreXml;
	}
	
	@Cacheable(cacheNames = "SCORE_XML")
	public String get(Integer eventId) {
		return null;
	}

}