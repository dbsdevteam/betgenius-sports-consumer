package com.donbest.betgenius.consumer.cache;

import com.donbest.betgenius.hattrick.model.EventRotation;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FixtureCache {
	
	@CachePut(cacheNames = "FIXTURE_CACHE", key = "#matchId")
	public EventRotation put(String matchId, EventRotation eventRot) {
		return eventRot;
	}
	
	@Cacheable(cacheNames = "FIXTURE_CACHE")
	public EventRotation get(String matchId) {
		return null;
	}
	
	@CacheEvict(cacheNames = "FIXTURE_CACHE", key = "#matchId")
	public void evictEventRot(String matchId){}

}