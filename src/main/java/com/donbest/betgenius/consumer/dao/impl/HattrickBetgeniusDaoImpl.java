package com.donbest.betgenius.consumer.dao.impl;

import java.util.Date;

import com.donbest.betgenius.consumer.model.common.League;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.donbest.betgenius.consumer.dao.HattrickBetgeniusDao;
import com.donbest.betgenius.hattrick.model.EventRotation;

@Component
public class HattrickBetgeniusDaoImpl implements HattrickBetgeniusDao {

	public final JdbcTemplate jdbcTemplate;

	public HattrickBetgeniusDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public EventRotation findEventRotationFromMatchId (String matchId) {
		final String sql = "SELECT event_id, "
						 + "rotation_id, "
						 + "betgenius_away_team_name, "
						 + "betgenius_home_team_name, "
						 + "betgenius_open_time, "
						 + "league_id, "
						 + "flip_flag "
						 + "FROM betgenius_event "
						 + "WHERE betgenius_match_id = ?";
		
		RowMapper<EventRotation> mapper = (rs, rowNum) -> {
			EventRotation et = new EventRotation();
			Integer rotationId= rs.getInt("rotation_id");
			et.setEventId(rs.getInt("event_id"));
			et.setAwayRot(rotationId);
			et.setAwayTeam(rs.getString("betgenius_away_team_name"));
			et.setHomeTeam(rs.getString("betgenius_home_team_name"));
			et.setLeagueId(rs.getInt("league_id"));
			et.setFlip(rs.getBoolean("flip_flag"));
			et.setStartTime(rs.getTimestamp("betgenius_open_time").getTime());
			return et;
		};
		try { 
			return jdbcTemplate.queryForObject(sql, mapper, matchId);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int insertOrUpdateMatchIdWhenCountZeroAndEventRotNull(String matchId, String awayTeamName, String homeTeamName,	League league, Date scheduled_time, Date timestamp) {
		return jdbcTemplate.update(
				"INSERT INTO betgenius_event "
					+ "(betgenius_match_id, "
					+ "betgenius_away_team_name, "
					+ "betgenius_home_team_name, "
					+ "betgenius_sport,"
					+ "betgenius_category, "
					+ "league_id, "
					+ "betgenius_open_time, "
					+ "timestamp, "
					+ "event_id, "
					+ "rotation_id) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL)"
					+ "ON DUPLICATE KEY UPDATE "
					+ "betgenius_away_team_name = VALUES(betgenius_away_team_name),"
					+ "betgenius_home_team_name = VALUES(betgenius_home_team_name),"
					+ "betgenius_sport = VALUES(betgenius_sport),"
					+ "betgenius_category = VALUES(betgenius_category),"
					+ "league_id = VALUES(league_id),"
					+ "betgenius_open_time = VALUES(betgenius_open_time),"
					+ "timestamp = VALUES(timestamp)",
					matchId, awayTeamName, homeTeamName, league.getSport(), league.getCategory(), league.getId(), scheduled_time, timestamp);
	}

	@Override
	public int insertOrUpdateMatchIdWhenCountZeroAndEventRotNotNull(String matchId, String awayTeamName, String homeTeamName, League league, Date scheduled_time, EventRotation eventRotation, Date timestamp) {
		return jdbcTemplate.update(
				"INSERT INTO betgenius_event "
					+ "(betgenius_match_id, "
					+ "betgenius_away_team_name, "
					+ "betgenius_home_team_name, "
					+ "betgenius_sport, "
					+ "betgenius_category, "
					+ "betgenius_open_time, "
					+ "event_id, "
					+ "rotation_id, "
					+ "league_id, "
					+ "flip_flag, "
					+ "timestamp) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
					+ "ON DUPLICATE KEY UPDATE "
					+ "event_id = VALUES(event_id),"
					+ "rotation_id = VALUES(rotation_id),"
					+ "league_id = VALUES(league_id),"
					+ "flip_flag = VALUES(flip_flag),"
					+ "timestamp = VALUES(timestamp)",
					matchId, awayTeamName, homeTeamName, league.getSport(), league.getCategory(), scheduled_time,
					eventRotation.getEventId(), eventRotation.getAwayRot(), eventRotation.getLeagueId(),
					eventRotation.isFlip(), timestamp);
	}

	@Override
	public int deleteMatchIdFromDatabase(String matchId) {
		return jdbcTemplate.update(
				"DELETE FROM betgenius_event "
					+ "WHERE betgenius_match_id = ?",
					matchId);
	}

}