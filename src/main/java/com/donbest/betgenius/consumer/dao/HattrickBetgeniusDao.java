package com.donbest.betgenius.consumer.dao;

import java.util.Date;

import com.donbest.betgenius.consumer.model.common.League;
import com.donbest.betgenius.hattrick.model.EventRotation;

public interface HattrickBetgeniusDao {

	EventRotation findEventRotationFromMatchId (String matchId);

	int deleteMatchIdFromDatabase(String matchId);

	int insertOrUpdateMatchIdWhenCountZeroAndEventRotNotNull(String matchId, String awayTeamName, String homeTeamName, League league, Date scheduled_time, EventRotation eventRotation, Date timestamp);

	int insertOrUpdateMatchIdWhenCountZeroAndEventRotNull(String matchId, String awayTeamName, String homeTeamName, League league, Date scheduled_time, Date timestamp);

}
