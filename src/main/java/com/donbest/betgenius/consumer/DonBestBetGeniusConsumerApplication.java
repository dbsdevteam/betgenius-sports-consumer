package com.donbest.betgenius.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableConfigurationProperties
@EnableCaching
public class DonBestBetGeniusConsumerApplication {
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	public static void main(String[] args) {
		SpringApplication.run(DonBestBetGeniusConsumerApplication.class, args);
	}

}
