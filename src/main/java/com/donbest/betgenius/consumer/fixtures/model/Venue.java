package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Venue{
    public String deleted;
    public String venueType;
    public String regionId;
    public Links _links;
    public String lastUpdate;
    public String name;
    public String neutral;
    public Venueproperty venueproperty;
    public String id;
    public String updates;
}
