package com.donbest.betgenius.consumer.fixtures.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Fixture{
    public Venue venue;
    public String competitionId;
    public String statusType;
    public ArrayList<Fixturecompetitor> fixturecompetitors;
    public Links _links;
    public String competitionName;
    public String roundName;
    public Fixtureproperty fixtureproperty;
    public String type;
    public String updates;
    public String sportId;
    public String deleted;
    public String seasonId;
    public String seasonName;
    public String lastUpdate;
    public String name;
    public String sportName;
    public String id;
    public String roundId;
    public String startDate;

}
