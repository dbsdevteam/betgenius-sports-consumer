package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Playerproperty{
    public String owner;
    public String lastName;
    public String nickName;
    public String weight;
    public String fullDescription;
    public String dam;
    public String firstName;
    public String horseSex;
    public String dob;
    public String trainer;
    public String position;
    public String sire;
    public String shortName;
    public String height;
    public String status;
    public String hand;
    public String isProtected;
}
