package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cury{
    public String templated;
    public String name;
    public String href;
}