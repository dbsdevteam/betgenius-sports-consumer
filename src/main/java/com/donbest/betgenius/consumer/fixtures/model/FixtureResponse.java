package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixtureResponse{
    public String total;
    public Links _links;
    public Embedded _embedded;
}