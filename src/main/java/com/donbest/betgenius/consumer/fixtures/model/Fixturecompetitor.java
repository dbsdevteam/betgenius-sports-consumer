package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fixturecompetitor{
    public String number;
    public String deleted;
    public Links _links;
    public Competitor competitor;
    public String lastUpdate;
    public String id;
    public String updates;
    public Fixturecompetitorproperty fixturecompetitorproperty;
}
