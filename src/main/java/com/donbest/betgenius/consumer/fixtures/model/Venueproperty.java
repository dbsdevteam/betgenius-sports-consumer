package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Venueproperty{
    public String par;
    public String surface;
    public String built;
    public String city;
    public String fieldDimensions;
    public String latitude;
    public String architect;
    public String length;
    public String opened;
    public String capacity;
    public String location;
    public String longitude;
    public String constructionCost;
}
