package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Teamproperty{
    public String awayShirtSecondary;
    public String conference;
    public String toBeDecided;
    public String awayShirtPrimary;
    public String ageCategory;
    public String homePage;
    public String startingPitcherId;
    public String shortName;
    public String fullDescription;
    public String youth;
    public String special;
    public String division;
    public String homeShirtSecondary;
    public String isProtected;
    public String homeShirtPrimary;
    public String reserve;
    public String comment;
    public String isNationalTeam;
}
