package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Competitor{
    public String deleted;
    public String gender;
    public Links _links;
    public String lastUpdate;
    public String name;
    public String id;
    public String updates;
    public String sportId;
    public String type;
    public Teamproperty teamproperty;
    public Playerproperty playerproperty;
}