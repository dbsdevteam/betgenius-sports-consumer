package com.donbest.betgenius.consumer.fixtures.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fixtureproperty{
    public String raceType;
    public String regularHalfDuration;
    public String distance;
    public String timezone;
    public String groundQuality;
    public String neutralField;
    public String awayGoalsAfterNormalTime;
    public String fixtureFormat;
    public String isProtected;
    public String unitsOfPlay;
    public String bestOf;
    public String bestOfNum;
    public String extraTime;
    public String sets;
    public String surface;
    public String hasLineups;
    public String finalSet;
    public String handicapRace;
    public String extraTimeHalfDuration;
    public String round;
    public String matchDuration;
    public String goesStraightToPenalties;
    public String prizeMoney;
    public String scoringSystem;
    public String awayGoalsAfterExtraTime;
    public String association;
    public String varRule;
    public String overtimePeriodDuration;
    public String overtimeType;
    public String skatersOnIce;
    public String rotationCodeHome;
    public String rotationCodeAway;
    public String finalSetTieBreakStarts;
    public String finalSetTieBreakWinnerFirst;
    public String federation;
    @JsonProperty("FirstLegId") 
    public String firstLegId;
    @JsonProperty("SecondLegId") 
    public String secondLegId;
    public String penaltyShootouts;
    public String numberOfInnings;
    public String numberOfExtraInnings;
    public String extraInningsRunnerOnSecondBase;
}
