package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Self{
    public String templated;
    public String href;
}
