package com.donbest.betgenius.consumer.fixtures.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Embedded{
    public ArrayList<Fixture> fixtures;
}
