package com.donbest.betgenius.consumer.fixtures.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fixturecompetitorproperty{
    public String form;
    public String saddleNo;
    public String winnerOfEventId;
    public String tackle;
    public String silkDescription;
    public String gateNo;
    public String jockey;
    public String weight;
    public String startingPitcher;
}
