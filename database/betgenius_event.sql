CREATE TABLE `betgenius_event` (
     `betgenius_match_id` varchar(255) NOT NULL DEFAULT '',
     `betgenius_away_team_name` varchar(255) DEFAULT NULL,
     `betgenius_home_team_name` varchar(255) DEFAULT NULL,
     `betgenius_sport` varchar(255) DEFAULT NULL,
     `betgenius_category` varchar(255) DEFAULT NULL,
     `betgenius_open_time` datetime DEFAULT NULL,
     `event_id` int(11) DEFAULT NULL,
     `rotation_id` int(11) DEFAULT NULL,
     `league_id` int(11) DEFAULT NULL,
     `flip_flag` tinyint(1) DEFAULT '1',
     `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     PRIMARY KEY (`betgenius_match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;